package com.kozlovsky.pharmacy.entity;

import java.math.BigDecimal;

public class Drug extends Entity{
    private String name;
    private String description;
    private boolean requireRecipe;
    private BigDecimal price;
    private String initialDose;
    private String maintenseDose;
    private long groupId;
    private long producerId;

    public Drug() {
    }

    public Drug(long id, String name, String description, boolean requireRecipe, BigDecimal price, String initialDose, String maintenseDose, long groupId, long producerId) {
        super.setId(id);
        this.name = name;
        this.description = description;
        this.requireRecipe = requireRecipe;
        this.price = price;
        this.initialDose = initialDose;
        this.maintenseDose = maintenseDose;
        this.groupId = groupId;
        this.producerId = producerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getRequireRecipe() {
        return requireRecipe;
    }

    public void setRequireRecipe(boolean requireRecipe) {
        this.requireRecipe = requireRecipe;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getInitialDose() {
        return initialDose;
    }

    public void setInitialDose(String initialDose) {
        this.initialDose = initialDose;
    }

    public String getMaintenseDose() {
        return maintenseDose;
    }

    public void setMaintenseDose(String maintenseDose) {
        this.maintenseDose = maintenseDose;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public long getProducerId() {
        return producerId;
    }

    public void setProducerId(long producerId) {
        this.producerId = producerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Drug drug = (Drug) o;

        if (requireRecipe != drug.requireRecipe) return false;
        if (groupId != drug.groupId) return false;
        if (producerId != drug.producerId) return false;
        if (name != null ? !name.equals(drug.name) : drug.name != null) return false;
        if (description != null ? !description.equals(drug.description) : drug.description != null) return false;
        if (price != null ? !price.equals(drug.price) : drug.price != null) return false;
        if (initialDose != null ? !initialDose.equals(drug.initialDose) : drug.initialDose != null) return false;
        return maintenseDose != null ? maintenseDose.equals(drug.maintenseDose) : drug.maintenseDose == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (requireRecipe ? 1 : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (initialDose != null ? initialDose.hashCode() : 0);
        result = 31 * result + (maintenseDose != null ? maintenseDose.hashCode() : 0);
        result = 31 * result + (int) (groupId ^ (groupId >>> 32));
        result = 31 * result + (int) (producerId ^ (producerId >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Drug{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", requireRecipe=" + requireRecipe +
                ", price=" + price +
                ", initialDose='" + initialDose + '\'' +
                ", maintenseDose='" + maintenseDose + '\'' +
                ", groupId=" + groupId +
                ", producerId=" + producerId +
                '}';
    }
}
