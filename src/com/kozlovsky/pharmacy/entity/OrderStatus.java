package com.kozlovsky.pharmacy.entity;

public enum OrderStatus {
    NEW,
    IN_PROCESS,
    PROCESSED,
    REJECTED
}