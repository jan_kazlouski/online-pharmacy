package com.kozlovsky.pharmacy.entity;

import java.sql.Date;

public class Recipe extends Entity {
    private Date dateOfBeginning;
    private Date dateOfEnding;
    private int quantity;
    private long clientId;
    private long doctorId;
    private long drugId;

    public Recipe(long id, Date dateOfBeginning, Date dateOfEnding, int quantity, long clientId, long doctorId, long drugId) {
        super.setId(id);
        this.dateOfBeginning = dateOfBeginning;
        this.dateOfEnding = dateOfEnding;
        this.quantity = quantity;
        this.clientId = clientId;
        this.doctorId = doctorId;
        this.drugId = drugId;
    }

    public Recipe() {
    }

    public Date getDateOfBeginning() {
        return dateOfBeginning;
    }

    public void setDateOfBeginning(Date dateOfBeginning) {
        this.dateOfBeginning = dateOfBeginning;
    }

    public Date getDateOfEnding() {
        return dateOfEnding;
    }

    public void setDateOfEnding(Date dateOfEnding) {
        this.dateOfEnding = dateOfEnding;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(long doctorId) {
        this.doctorId = doctorId;
    }

    public long getDrugId() {
        return drugId;
    }

    public void setDrugId(long drugId) {
        this.drugId = drugId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Recipe recipe = (Recipe) o;

        if (quantity != recipe.quantity) return false;
        if (clientId != recipe.clientId) return false;
        if (doctorId != recipe.doctorId) return false;
        if (drugId != recipe.drugId) return false;
        if (dateOfBeginning != null ? !dateOfBeginning.equals(recipe.dateOfBeginning) : recipe.dateOfBeginning != null)
            return false;
        return dateOfEnding != null ? dateOfEnding.equals(recipe.dateOfEnding) : recipe.dateOfEnding == null;
    }

    @Override
    public int hashCode() {
        int result = dateOfBeginning != null ? dateOfBeginning.hashCode() : 0;
        result = 31 * result + (dateOfEnding != null ? dateOfEnding.hashCode() : 0);
        result = 31 * result + quantity;
        result = 31 * result + (int) (clientId ^ (clientId >>> 32));
        result = 31 * result + (int) (doctorId ^ (doctorId >>> 32));
        result = 31 * result + (int) (drugId ^ (drugId >>> 32));
        return result;
    }
}
