package com.kozlovsky.pharmacy.entity;

public enum UserCategory {
    CLIENT,
    PHARMACIST,
    DOCTOR,
    ADMIN
}