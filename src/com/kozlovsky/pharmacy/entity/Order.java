package com.kozlovsky.pharmacy.entity;

import java.math.BigDecimal;
import java.sql.Date;

public class Order extends Entity {
    private int quantity;
    private Date orderDate;
    private BigDecimal price;
    private long userId;
    private long drugId;
    private OrderStatus orderStatus;

    public Order() {
    }

    public Order(long id, int quantity, Date orderDate, BigDecimal price, long userId, long drugId, OrderStatus orderStatus) {
        super.setId(id);
        this.quantity = quantity;
        this.orderDate = orderDate;
        this.price = price;
        this.userId = userId;
        this.drugId = drugId;
        this.orderStatus = orderStatus;
    }


    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getDrugId() {
        return drugId;
    }

    public void setDrugId(long drugId) {
        this.drugId = drugId;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (quantity != order.quantity) return false;
        if (userId != order.userId) return false;
        if (drugId != order.drugId) return false;
        if (orderDate != null ? !orderDate.equals(order.orderDate) : order.orderDate != null) return false;
        if (price != null ? !price.equals(order.price) : order.price != null) return false;
        return orderStatus == order.orderStatus;
    }

    @Override
    public int hashCode() {
        int result = quantity;
        result = 31 * result + (orderDate != null ? orderDate.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (int) (userId ^ (userId >>> 32));
        result = 31 * result + (int) (drugId ^ (drugId >>> 32));
        result = 31 * result + (orderStatus != null ? orderStatus.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Order{" +
                "quantity=" + quantity +
                ", orderDate=" + orderDate +
                ", price=" + price +
                ", userId=" + userId +
                ", drugId=" + drugId +
                ", orderStatus=" + orderStatus +
                '}';
    }
}
