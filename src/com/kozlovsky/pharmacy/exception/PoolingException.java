package com.kozlovsky.pharmacy.exception;

public class PoolingException extends Exception{
    public PoolingException() {
    }

    public PoolingException(String message) {
        super(message);
    }

    public PoolingException(String message, Throwable cause) {
        super(message, cause);
    }

    public PoolingException(Throwable cause) {
        super(cause);
    }
}
