package com.kozlovsky.pharmacy.command;

import com.kozlovsky.pharmacy.entity.UserCategory;

public abstract class ClientCommand implements Command{

    @Override
    public boolean checkAccess(UserCategory category) {
        return (category!=null && UserCategory.CLIENT.equals(category));
    }
}
