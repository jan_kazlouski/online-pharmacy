package com.kozlovsky.pharmacy.command;

import com.kozlovsky.pharmacy.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

public class ChangeLanguageCommand extends CommonCommand{
    private static final String LANGUAGE = "language";
    private static final String PAGE = "page";
    private static final String LOCALE = "locale";
    @Override
    public String execute(HttpServletRequest request) {
        String language = request.getParameter(LANGUAGE);
        request.getSession().setAttribute(LOCALE, language);
        return ConfigurationManager.getProperty((String) request.getSession().getAttribute(PAGE));
    }
}
