package com.kozlovsky.pharmacy.command.order;

import com.kozlovsky.pharmacy.command.ClientCommand;
import com.kozlovsky.pharmacy.exception.LogicException;
import com.kozlovsky.pharmacy.logic.OrderLogic;
import com.kozlovsky.pharmacy.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

public class AddOrderCommand extends ClientCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        try{
            long clientId = (long)request.getSession().getAttribute("user_id");
            long drugId = Long.parseLong(request.getParameter("drug_id"));
            int quantity = Integer.parseInt(request.getParameter("quantity"));
            boolean result = OrderLogic.addOrder(quantity,clientId,drugId);
            if(result){
                page = ConfigurationManager.getProperty("path.page.main");
            }else {
                page = ConfigurationManager.getProperty("path.page.add_drug");
            }
        }catch (LogicException | NumberFormatException e){
            page = ConfigurationManager.getProperty("path.page.error");
        }
        return page;
    }
}
