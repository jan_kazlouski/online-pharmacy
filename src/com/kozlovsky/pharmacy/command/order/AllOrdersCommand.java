package com.kozlovsky.pharmacy.command.order;

import com.kozlovsky.pharmacy.command.AdminCommand;
import com.kozlovsky.pharmacy.entity.Order;
import com.kozlovsky.pharmacy.exception.LogicException;
import com.kozlovsky.pharmacy.logic.OrderLogic;
import com.kozlovsky.pharmacy.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class AllOrdersCommand extends AdminCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        try {
            List<Order> allOrders = OrderLogic.takeAllOrders();
            request.setAttribute("orders", allOrders);
            page = ConfigurationManager.getProperty("path.page.orders");
        } catch (LogicException e) {
            page = ConfigurationManager.getProperty("path.page.error");
        }
        return page;
    }
}
