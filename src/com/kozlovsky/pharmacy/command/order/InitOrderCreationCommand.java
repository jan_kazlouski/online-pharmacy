package com.kozlovsky.pharmacy.command.order;

import com.kozlovsky.pharmacy.command.ClientCommand;
import com.kozlovsky.pharmacy.entity.Drug;
import com.kozlovsky.pharmacy.exception.LogicException;
import com.kozlovsky.pharmacy.logic.DrugLogic;
import com.kozlovsky.pharmacy.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

public class InitOrderCreationCommand extends ClientCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        try {
            long drugId = Long.parseLong(request.getParameter("drug_id"));
            Drug drug = DrugLogic.takeDrug(drugId);
            request.setAttribute("drug", drug);
            page = ConfigurationManager.getProperty("path.page.add_order");
        } catch (LogicException e) {
            page = ConfigurationManager.getProperty("path.page.error");
        }
        return page;
    }
}
