package com.kozlovsky.pharmacy.command.user;

import com.kozlovsky.pharmacy.command.AdminCommand;
import com.kozlovsky.pharmacy.command.Command;
import com.kozlovsky.pharmacy.exception.LogicException;
import com.kozlovsky.pharmacy.logic.UserLogic;
import com.kozlovsky.pharmacy.resource.ConfigurationManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class DeleteUserCommand extends AdminCommand{

    private static Logger LOGGER = LogManager.getLogger(DeleteUserCommand.class);

    @Override
    public String execute(HttpServletRequest request){
        String page;
        try {
            long adminId = (Long)request.getSession().getAttribute("user_id");
            long userId = Long.parseLong(request.getParameter("id"));
            boolean result = UserLogic.deleteUser(adminId, userId);
            if (!result) {
                throw new LogicException("Can't delete user " + userId);
            }
            page = ConfigurationManager.getProperty("path.page.user_deleted");
        }catch (LogicException e){
            LOGGER.log(Level.ERROR,e);
            page = ConfigurationManager.getProperty("path.page.error");
        }
        return page;
    }
}
