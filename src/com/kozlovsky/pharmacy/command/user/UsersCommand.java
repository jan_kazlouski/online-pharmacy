package com.kozlovsky.pharmacy.command.user;

import com.kozlovsky.pharmacy.command.AdminCommand;
import com.kozlovsky.pharmacy.command.Command;
import com.kozlovsky.pharmacy.entity.User;
import com.kozlovsky.pharmacy.exception.LogicException;
import com.kozlovsky.pharmacy.logic.UserLogic;
import com.kozlovsky.pharmacy.resource.ConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class UsersCommand extends AdminCommand{
    private static Logger log = LogManager.getLogger(UsersCommand.class);

    private static final String USERS = "users";

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        try {
            List<User> userList = UserLogic.takeAllUsersExceptAdmins();
            request.setAttribute(USERS, userList);
            page = ConfigurationManager.getProperty("path.page.users");
        } catch (LogicException e) {
            log.error(e);
            page = ConfigurationManager.getProperty("path.page.error");
        }
        return page;
    }
}
