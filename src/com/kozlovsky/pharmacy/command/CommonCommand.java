package com.kozlovsky.pharmacy.command;

import com.kozlovsky.pharmacy.entity.UserCategory;

public abstract class CommonCommand implements Command{

    @Override
    public boolean checkAccess(UserCategory category) {
        return true;
    }
}
