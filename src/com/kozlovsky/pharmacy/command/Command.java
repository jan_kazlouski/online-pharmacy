package com.kozlovsky.pharmacy.command;

import com.kozlovsky.pharmacy.entity.UserCategory;

import javax.servlet.http.HttpServletRequest;

public interface Command {
    String execute(HttpServletRequest request);
    public abstract boolean checkAccess(UserCategory category);
}
