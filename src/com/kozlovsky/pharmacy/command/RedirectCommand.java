package com.kozlovsky.pharmacy.command;

import com.kozlovsky.pharmacy.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

public class RedirectCommand extends CommonCommand{

    private static final String NEXT_PAGE = "nextPage";
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        String nextPage = request.getParameter(NEXT_PAGE);
        page = ConfigurationManager.getProperty(nextPage);
        return page;
    }
}
