package com.kozlovsky.pharmacy.command.recipe;


import com.kozlovsky.pharmacy.command.DoctorCommand;
import com.kozlovsky.pharmacy.exception.LogicException;
import com.kozlovsky.pharmacy.logic.RecipeLogic;
import com.kozlovsky.pharmacy.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;

public class AddRecipeCommand extends DoctorCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        try{
            int quantity = Integer.parseInt(request.getParameter("quantity"));
            long doctorId = (long)request.getSession().getAttribute("user_id");
            long clientId = Long.parseLong(request.getParameter("client_id"));
            int drugId = Integer.parseInt(request.getParameter("drug_id"));
            Date dateOfBeginning = Date.valueOf(request.getParameter("beginning"));
            Date dateOfEnding = Date.valueOf(request.getParameter("ending"));
            boolean result = RecipeLogic.addRecipe(dateOfBeginning,dateOfEnding,quantity,clientId,doctorId,drugId);
            if(result){
                page = ConfigurationManager.getProperty("path.page.main");
            }else {
                page = ConfigurationManager.getProperty("path.page.add_recipe");
            }
        }catch (LogicException | NumberFormatException e){
            page = ConfigurationManager.getProperty("path.page.error");
        }
        return page;
    }
}
