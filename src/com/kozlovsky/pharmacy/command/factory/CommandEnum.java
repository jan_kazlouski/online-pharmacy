package com.kozlovsky.pharmacy.command.factory;

import com.kozlovsky.pharmacy.command.ChangeLanguageCommand;
import com.kozlovsky.pharmacy.command.Command;
import com.kozlovsky.pharmacy.command.EmptyCommand;
import com.kozlovsky.pharmacy.command.RedirectCommand;
import com.kozlovsky.pharmacy.command.drug.*;
import com.kozlovsky.pharmacy.command.order.*;
import com.kozlovsky.pharmacy.command.recipe.AddRecipeCommand;
import com.kozlovsky.pharmacy.command.session.LoginCommand;
import com.kozlovsky.pharmacy.command.session.LogoutCommand;
import com.kozlovsky.pharmacy.command.session.RegisterCommand;
import com.kozlovsky.pharmacy.command.user.DeleteUserCommand;
import com.kozlovsky.pharmacy.command.user.UsersCommand;

public enum CommandEnum {
    EMPTY{
        {
            this.command = new EmptyCommand();
        }
    },
    REDIRECT{
        {
            this.command = new RedirectCommand();
        }
    },
    LOGIN{
        {
            this.command = new LoginCommand();
        }
    },
    LOGOUT{
        {
            this.command = new LogoutCommand();
        }
    },
    REGISTER{
        {
            this.command = new RegisterCommand();
        }
    },
    CHANGE_LANGUAGE{
        {
            this.command = new ChangeLanguageCommand();
        }
    },
    ALL_DRUGS{
        {
            this.command = new AllDrugsCommand();
        }
    },
    AVAILABLE_DRUGS{
        {
            this.command = new AvailableDrugsCommand();
        }
    },
    SHOW_DRUG{
        {
            this.command = new ShowDrugCommand();
        }
    },
    ADD_DRUG{
        {
            this.command = new AddDrugCommand();
        }
    },
    INIT_EDIT_DRUG{
        {
            this.command = new InitEditDrugCommand();
        }
    },
    EDIT_DRUG{
        {
            this.command = new EditDrugCommand();
        }
    },
    DELETE_DRUG{
        {
            this.command = new DeleteDrugCommand();
        }
    },
    USERS{
        {
            this.command = new UsersCommand();
        }
    },
    DELETE_USER{
        {
            this.command = new DeleteUserCommand();
        }
    },
    INIT_ORDER{
        {
            this.command = new InitOrderCreationCommand();
        }
    },
    ADD_ORDER{
        {
            this.command = new AddOrderCommand();
        }
    },
    ADD_RECIPE{
        {
            this.command = new AddRecipeCommand();
        }
    },
    ALL_ORDERS{
        {
            this.command = new AllOrdersCommand();
        }
    },
    ACTIVE_ORDERS{
        {
            this.command = new ActiveOrdersCommand();
        }
    },
    PROCESS_ORDER{
        {
            this.command = new ProcessOrderCommand();
        }
    },
    FINISH_ORDER{
        {
            this.command = new FinishOrderCommand();
        }
    },
    CANCEL_ORDER{
        {
            this.command = new CancelOrderCommand();
        }
    };

    Command command;

    public Command getCommand() {
        return command;
    }

}
