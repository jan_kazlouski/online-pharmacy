package com.kozlovsky.pharmacy.command.factory;

import com.kozlovsky.pharmacy.command.Command;
import com.kozlovsky.pharmacy.command.EmptyCommand;
import com.kozlovsky.pharmacy.resource.MessageManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class ActionFactory {

    private static final Logger LOGGER = LogManager.getLogger(ActionFactory.class);

    public Command defineCommand(HttpServletRequest request){
        Command current = new EmptyCommand();
        String action = request.getParameter("command");
        if(action == null || action.isEmpty()){
            return current;
        }
        try {
            CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
            current = currentEnum.getCommand();
        } catch (IllegalArgumentException e) {
            request.setAttribute("wrongAction", action
                    + MessageManager.getProperty("message.wrongaction"));
        }
        return current;
    }
}
