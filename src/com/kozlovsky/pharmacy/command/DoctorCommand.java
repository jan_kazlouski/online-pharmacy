package com.kozlovsky.pharmacy.command;

import com.kozlovsky.pharmacy.entity.UserCategory;

public abstract class DoctorCommand implements Command{

    @Override
    public boolean checkAccess(UserCategory category) {
        return (category!=null && UserCategory.DOCTOR.equals(category));
    }
}
