package com.kozlovsky.pharmacy.command;

import com.kozlovsky.pharmacy.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

public class EmptyCommand extends CommonCommand{

    private static final String PAGE_LOGIN = "path.page.login";
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty(PAGE_LOGIN);
        return page;
    }
}
