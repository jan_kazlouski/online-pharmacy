package com.kozlovsky.pharmacy.command.session;

import com.kozlovsky.pharmacy.command.Command;
import com.kozlovsky.pharmacy.command.CommonCommand;
import com.kozlovsky.pharmacy.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

public class LogoutCommand extends CommonCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.index");
        request.getSession().invalidate();
        return page;
    }
}
