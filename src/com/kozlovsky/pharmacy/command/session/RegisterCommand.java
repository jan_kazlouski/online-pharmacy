package com.kozlovsky.pharmacy.command.session;

import com.kozlovsky.pharmacy.command.Command;
import com.kozlovsky.pharmacy.command.CommonCommand;
import com.kozlovsky.pharmacy.entity.User;
import com.kozlovsky.pharmacy.logic.RegistrationResult;
import com.kozlovsky.pharmacy.logic.UserLogic;
import com.kozlovsky.pharmacy.exception.LogicException;

import com.kozlovsky.pharmacy.resource.ConfigurationManager;
import com.kozlovsky.pharmacy.resource.MessageManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class RegisterCommand extends CommonCommand {

    private static Logger LOGGER = LogManager.getLogger(RegisterCommand.class);
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String REPEAT_PASSWORD = "repeat_password";
    private static final String EMAIL = "email";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String MIDDLE_NAME = "middle_name";
    private static final String USER_ID = "user_id";
    private static final String CATEGORY = "category";
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        String login = request.getParameter(LOGIN);
        String password = request.getParameter(PASSWORD);
        String repeatPassword = request.getParameter(REPEAT_PASSWORD);
        String email = request.getParameter(EMAIL);
        String firstName = request.getParameter(FIRST_NAME);
        String lastName = request.getParameter(LAST_NAME);
        String middleName = request.getParameter(MIDDLE_NAME);
        User user = new User();
        try{
            RegistrationResult result = UserLogic.register(login,password,repeatPassword,firstName,middleName,lastName,email,user);
            switch (result){
                case ALL_RIGHT:
                    request.getSession().setAttribute(USER_ID,user.getId());
                    request.getSession().setAttribute(CATEGORY,user.getUserCategory());
                    page = ConfigurationManager.getProperty("path.page.main");
                    break;
                case LOGIN_PASS_INCORRECT:
                    request.setAttribute("errorLoginPassMessage", MessageManager.getProperty("message.loginerror"));
                    break;
                case EMAIL_INCORRECT:
                    request.setAttribute("errorLoginPassMessage", MessageManager.getProperty("message.emailerror"));
                    break;
                case PASS_NOT_MATCH:
                    request.setAttribute("errorLoginPassMessage", MessageManager.getProperty("message.passnotmatch"));
                    break;
                case LOGIN_NOT_UNIQUE:
                    request.setAttribute("errorLoginPassMessage", MessageManager.getProperty("message.loginduplicate"));
                    break;
                case EMAIL_NOT_UNIQUE:
                    request.setAttribute("errorLoginPassMessage", MessageManager.getProperty("message.emailduplicate"));
                    break;
                case NAME_INCORRECT:
                    request.setAttribute("errorLoginPassMessage", MessageManager.getProperty("message.nameerror"));
                    break;
            }
            if (result != RegistrationResult.ALL_RIGHT) {
                request.setAttribute(LOGIN, login);
                request.setAttribute(PASSWORD, password);
                request.setAttribute(REPEAT_PASSWORD, repeatPassword);
                request.setAttribute(EMAIL, email);
                page = ConfigurationManager.getProperty("path.page.register");
            }
        }catch (NumberFormatException | LogicException e){
            LOGGER.log(Level.ERROR,e);
            page= ConfigurationManager.getProperty("path.page.register");
        }
        return page;
    }
}
