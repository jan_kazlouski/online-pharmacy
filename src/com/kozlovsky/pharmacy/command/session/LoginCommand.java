package com.kozlovsky.pharmacy.command.session;

import com.kozlovsky.pharmacy.command.Command;
import com.kozlovsky.pharmacy.command.CommonCommand;
import com.kozlovsky.pharmacy.entity.User;
import com.kozlovsky.pharmacy.exception.LogicException;
import com.kozlovsky.pharmacy.logic.UserLogic;
import com.kozlovsky.pharmacy.resource.ConfigurationManager;
import com.kozlovsky.pharmacy.resource.MessageManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class LoginCommand extends CommonCommand{
    private static Logger LOGGER = LogManager.getLogger(LoginCommand.class);
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String USER_ID = "user_id";
    private static final String CATEGORY = "category";
    private static final String PAGE_MAIN = "path.page.main";
    private static final String PAGE_ERROR = "path.page.error";
    private static final String PAGE_LOGIN = "path.page.login";
    private static final String ERROR_LOGIN_MESSAGE = "errorLoginPassMessage";
    private static final String LOGIN_ERROR = "message.loginerror";
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        String login = request.getParameter(LOGIN);
        String password = request.getParameter(PASSWORD);
        User user;
        try {
            if((user = UserLogic.checkLogin(login,password))!=null){
                request.getSession().setAttribute(USER_ID,user.getId());
                request.getSession().setAttribute(CATEGORY,user.getUserCategory());
                page = ConfigurationManager.getProperty(PAGE_MAIN);
            }else{
                request.setAttribute(ERROR_LOGIN_MESSAGE, MessageManager.getProperty(LOGIN_ERROR));
                page = ConfigurationManager.getProperty(PAGE_LOGIN);
            }
        }catch (LogicException e){
            LOGGER.log(Level.ERROR,e);
            page = ConfigurationManager.getProperty(PAGE_ERROR);
        }
        return page;
    }
}
