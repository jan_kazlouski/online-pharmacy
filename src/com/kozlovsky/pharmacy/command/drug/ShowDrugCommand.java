package com.kozlovsky.pharmacy.command.drug;

import com.kozlovsky.pharmacy.command.Command;
import com.kozlovsky.pharmacy.command.CommonCommand;
import com.kozlovsky.pharmacy.entity.Drug;
import com.kozlovsky.pharmacy.entity.Group;
import com.kozlovsky.pharmacy.entity.Producer;
import com.kozlovsky.pharmacy.entity.User;
import com.kozlovsky.pharmacy.exception.LogicException;
import com.kozlovsky.pharmacy.logic.DrugLogic;
import com.kozlovsky.pharmacy.logic.GroupLogic;
import com.kozlovsky.pharmacy.logic.ProducerLogic;
import com.kozlovsky.pharmacy.logic.UserLogic;
import com.kozlovsky.pharmacy.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

public class ShowDrugCommand extends CommonCommand {
    private static final String DRUG = "drug";
    private static final String GROUP = "group";
    private static final String PRODUCER = "producer";
    private static final String DRUG_ID = "drug_id";

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        try {
            long drugId = Long.parseLong(request.getParameter(DRUG_ID));
            Drug drug = DrugLogic.takeDrug(drugId);
            Group group = GroupLogic.takeGroup(drugId);
            Producer producer = ProducerLogic.takeProducer(drugId);
            request.setAttribute(DRUG, drug);
            request.setAttribute(GROUP, group);
            request.setAttribute(PRODUCER, producer);
            page = ConfigurationManager.getProperty("path.page.current_drug");
        } catch (LogicException e) {
            page = ConfigurationManager.getProperty("path.page.error");
        }
        return page;
    }
}
