package com.kozlovsky.pharmacy.command.drug;

import com.kozlovsky.pharmacy.command.PharmacistCommand;
import com.kozlovsky.pharmacy.entity.Drug;
import com.kozlovsky.pharmacy.entity.Group;
import com.kozlovsky.pharmacy.entity.Producer;
import com.kozlovsky.pharmacy.exception.LogicException;
import com.kozlovsky.pharmacy.logic.DrugLogic;
import com.kozlovsky.pharmacy.logic.GroupLogic;
import com.kozlovsky.pharmacy.logic.ProducerLogic;
import com.kozlovsky.pharmacy.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

public class InitEditDrugCommand extends PharmacistCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        try {
            long drugId = Long.parseLong(request.getParameter("drug_id"));
            Drug drug = DrugLogic.takeDrug(drugId);
            Group group = GroupLogic.takeGroup(drugId);
            Producer producer = ProducerLogic.takeProducer(drugId);
            request.setAttribute("drug", drug);
            request.setAttribute("group", group);
            request.setAttribute("producer", producer);
            page = ConfigurationManager.getProperty("path.page.edit_drug");
        } catch (LogicException e) {
            page = ConfigurationManager.getProperty("path.page.error");
        }
        return page;
    }
}
