package com.kozlovsky.pharmacy.command.drug;

import com.kozlovsky.pharmacy.command.PharmacistCommand;
import com.kozlovsky.pharmacy.exception.LogicException;
import com.kozlovsky.pharmacy.logic.DrugLogic;
import com.kozlovsky.pharmacy.resource.ConfigurationManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class DeleteDrugCommand extends PharmacistCommand {

    private static Logger LOGGER = LogManager.getLogger(DeleteDrugCommand.class);
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        try {
            long drugId = Long.parseLong(request.getParameter("drug_id"));
            boolean result = DrugLogic.deleteDrug(drugId);
            if (!result) {
                throw new LogicException("Can't delete drug " + drugId);
            }
            page = ConfigurationManager.getProperty("path.page.drug_deleted");
        }catch (LogicException e){
            LOGGER.log(Level.ERROR,e);
            page = ConfigurationManager.getProperty("path.page.error");
        }
        return page;
    }
}
