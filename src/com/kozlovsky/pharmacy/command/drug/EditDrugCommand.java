package com.kozlovsky.pharmacy.command.drug;

import com.kozlovsky.pharmacy.command.Command;
import com.kozlovsky.pharmacy.command.PharmacistCommand;
import com.kozlovsky.pharmacy.entity.Drug;
import com.kozlovsky.pharmacy.exception.LogicException;
import com.kozlovsky.pharmacy.logic.DrugLogic;
import com.kozlovsky.pharmacy.logic.GroupLogic;
import com.kozlovsky.pharmacy.logic.ProducerLogic;
import com.kozlovsky.pharmacy.resource.ConfigurationManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.math.BigDecimal;

public class EditDrugCommand extends PharmacistCommand {
    private static final String DRUG_ID = "drug_id";
    private static final String NAME = "drug_name";
    private static final String DESCRIPTION = "description";
    private static final String REQUIRE_RECIPE = "require_recipe";
    private static final String PRICE = "price";
    private static final String INITIAL_DOSE = "initial_dose";
    private static final String MAINTENSE_DOSE = "maintense_dose";
    private static final String GROUP = "group";
    private static final String PRODUCER = "producer";
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        try {
            long drugId = Long.parseLong(request.getParameter(DRUG_ID));
            String name = request.getParameter(NAME);
            String description = request.getParameter(DESCRIPTION);
            Boolean requireRecipe = Boolean.parseBoolean(request.getParameter(REQUIRE_RECIPE));
            BigDecimal price = new BigDecimal(request.getParameter(PRICE));
            String initialDose = request.getParameter(INITIAL_DOSE);
            String maintenseDose = request.getParameter(MAINTENSE_DOSE);
            String group = request.getParameter(GROUP);
            String producer = request.getParameter(PRODUCER);
            Drug drug = DrugLogic.editDrug(drugId,name,description,requireRecipe,price,initialDose,maintenseDose,group,producer);
            request.setAttribute("drug", drug);
            request.setAttribute("group", GroupLogic.takeGroup(drugId));
            request.setAttribute("producer", ProducerLogic.takeProducer(drugId));
            page = ConfigurationManager.getProperty("path.page.current_drug");
        } catch (LogicException e) {
            page = ConfigurationManager.getProperty("path.page.error");
        }
        return page;
    }
}
