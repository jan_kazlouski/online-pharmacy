package com.kozlovsky.pharmacy.command.drug;

import com.kozlovsky.pharmacy.command.ClientCommand;
import com.kozlovsky.pharmacy.command.Command;
import com.kozlovsky.pharmacy.entity.Drug;
import com.kozlovsky.pharmacy.exception.LogicException;
import com.kozlovsky.pharmacy.logic.DrugLogic;
import com.kozlovsky.pharmacy.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class AvailableDrugsCommand extends ClientCommand{
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        try {
            List<Drug> availableDrugs = DrugLogic.takeAllDrugsAvailableToClient((Long)request.getSession().getAttribute("user_id"));
            request.setAttribute("drugs", availableDrugs);
            page = ConfigurationManager.getProperty("path.page.drugs");
        } catch (LogicException e) {
            page = ConfigurationManager.getProperty("path.page.error");
        }
        return page;
    }
}
