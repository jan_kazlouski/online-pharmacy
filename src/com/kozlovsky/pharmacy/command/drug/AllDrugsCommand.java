package com.kozlovsky.pharmacy.command.drug;

import com.kozlovsky.pharmacy.command.Command;
import com.kozlovsky.pharmacy.command.PharmacistCommand;
import com.kozlovsky.pharmacy.entity.Drug;
import com.kozlovsky.pharmacy.exception.LogicException;
import com.kozlovsky.pharmacy.logic.DrugLogic;
import com.kozlovsky.pharmacy.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class AllDrugsCommand extends PharmacistCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        try {
            List<Drug> allDrugs = DrugLogic.takeAllDrugs();
            request.setAttribute("drugs", allDrugs);
            page = ConfigurationManager.getProperty("path.page.drugs");
        } catch (LogicException e) {
            page = ConfigurationManager.getProperty("path.page.error");
        }
        return page;
    }
}
