package com.kozlovsky.pharmacy.command.drug;

import com.kozlovsky.pharmacy.command.PharmacistCommand;
import com.kozlovsky.pharmacy.exception.LogicException;
import com.kozlovsky.pharmacy.logic.DrugLogic;
import com.kozlovsky.pharmacy.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

public class AddDrugCommand extends PharmacistCommand{
    private static final String NAME = "name";
    private static final String DESCRIPTION = "description";
    private static final String REQUIRE_RECIPE = "require_recipe";
    private static final String PRICE = "price";
    private static final String INITIAL_DOSE = "initial_dose";
    private static final String MAINTENSE_DOSE = "maintense_dose";
    private static final String GROUP = "group";
    private static final String PRODUCER = "producer";

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        try{
            String name = request.getParameter(NAME);
            String description = request.getParameter(DESCRIPTION);
            Boolean requireRecipe = Boolean.parseBoolean(request.getParameter(REQUIRE_RECIPE));
            BigDecimal price = new BigDecimal(request.getParameter(PRICE));
            String initialDose = request.getParameter(INITIAL_DOSE);
            String maintenseDose = request.getParameter(MAINTENSE_DOSE);
            String group = request.getParameter(GROUP);
            String producer = request.getParameter(PRODUCER);
            boolean result = DrugLogic.addDrug(name,description,requireRecipe,price,initialDose,maintenseDose,group,producer);
            if(result){
                page = ConfigurationManager.getProperty("path.page.main");
            }else {
                page = ConfigurationManager.getProperty("path.page.add_drug");
            }
        }catch (LogicException | NumberFormatException e){
            page = ConfigurationManager.getProperty("path.page.error");
        }
        return page;
    }
}
