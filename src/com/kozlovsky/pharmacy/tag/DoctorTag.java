package com.kozlovsky.pharmacy.tag;

import com.kozlovsky.pharmacy.entity.UserCategory;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

@SuppressWarnings("serial")
public class DoctorTag extends TagSupport {
    private static final String CATEGORY = "category";

    @Override
    public int doStartTag() throws JspException {
        if (pageContext.getSession().getAttribute(CATEGORY) == UserCategory.DOCTOR) {
            return EVAL_BODY_INCLUDE;
        } else {
            return SKIP_BODY;
        }
    }

    @Override
    public int doAfterBody() throws JspTagException {
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
}