package com.kozlovsky.pharmacy.dao;

import com.kozlovsky.pharmacy.entity.Entity;
import com.kozlovsky.pharmacy.exception.DAOException;

public interface ManipulationDAO<K,T extends Entity> {
    boolean delete(T entity)throws DAOException;
    boolean delete(K id)throws DAOException;
    boolean create(T entity)throws DAOException;
    T update(T entity)throws DAOException;
}
