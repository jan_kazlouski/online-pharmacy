package com.kozlovsky.pharmacy.dao;

import com.kozlovsky.pharmacy.entity.Group;
import com.kozlovsky.pharmacy.exception.DAOException;
import com.kozlovsky.pharmacy.exception.PoolingException;
import com.kozlovsky.pharmacy.pooling.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class GroupDAO implements SearchDAO<Long,Group> {

    private static final String FIND_ALL_GROUPS = "SELECT `group_id`, `name` FROM `pharmacy`.`group`;";
    private static final String FIND_GROUP_BY_ID = "SELECT `group_id`, `name` FROM `pharmacy`.`group` WHERE `group`.`group_id` = ?;";
    private static final String FIND_GROUP_BY_DRUG_ID = "SELECT `group`.`group_id`, `group`.`name` FROM `pharmacy`.`group`, `pharmacy`.`drug` WHERE `drug`.`drug_id` = ? AND `drug`.`group_id` = `group`.`group_id`;";
    private static final String FIND_GROUP_BY_GROUP_NAME ="SELECT `group_id`, `name` FROM `pharmacy`.`group`WHERE `group`.`name`= ?;";
    @Override
    public List<Group> findAll() throws DAOException {
        return null;
    }

    @Override
    public Group findElementById(Long id) throws DAOException {
        return null;
    }

    public Group findGroupByDrugId(Long drugId) throws DAOException {
        Group result = null;
        try{
            Connection connection = ConnectionPool.getConnection();
            try(PreparedStatement statement = connection.prepareStatement(FIND_GROUP_BY_DRUG_ID)){
                statement.setLong(1,drugId);
                ResultSet resultSet = statement.executeQuery();
                if(resultSet.next()){
                    result=(new Group(
                            resultSet.getLong("group_id"),
                            resultSet.getString("name")));
                }
            }finally {
                ConnectionPool.returnConnection(connection);
            }
            return result;
        }catch (SQLException | PoolingException e){
            throw new DAOException(e);
        }
    }

    public Group findGroupByGroupName(String groupName) throws DAOException{
        Group result=null;
        try{
            Connection connection = ConnectionPool.getConnection();
            try(PreparedStatement statement = connection.prepareStatement(FIND_GROUP_BY_GROUP_NAME)){
                statement.setString(1,groupName);
                ResultSet resultSet = statement.executeQuery();
                if(resultSet.next()){
                    result=(new Group(
                            resultSet.getLong("group_id"),
                            resultSet.getString("name")));
                }
            }finally {
                ConnectionPool.returnConnection(connection);
            }
            return result;
        }catch (SQLException | PoolingException e){
            throw new DAOException(e);
        }
    }
}
