package com.kozlovsky.pharmacy.dao;

import com.kozlovsky.pharmacy.entity.User;
import com.kozlovsky.pharmacy.entity.UserCategory;
import com.kozlovsky.pharmacy.exception.DAOException;
import com.kozlovsky.pharmacy.exception.PoolingException;
import com.kozlovsky.pharmacy.pooling.ConnectionPool;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAO implements SearchDAO<Long,User>, ManipulationDAO<Long,User> {
    private static final String FIND_ALL_USERS = "SELECT `user_id`, `login`, `password`, `first_name`, `middle_name`, `last_name`, `email`, `user_category`.`name` AS `user_category` FROM `pharmacy`.`user`,`pharmacy`.`user_category` WHERE `user_category`.`user_category_id` = `user`.`user_category_id`;";
    private static final String FIND_USER_BY_ID = "SELECT `user_id`, `login`, `password`, `first_name`, `middle_name`, `last_name`, `email`, `user_category`.`name` AS `user_category` FROM `pharmacy`.`user`,`pharmacy`.`user_category` WHERE `user`.`user_id` = ? AND `user_category`.`user_category_id` = `user`.`user_category_id`;";
    private static final String FIND_USER_BY_LOGIN_PASSWORD = "SELECT `user_id`, `login`, `password`, `first_name`, `middle_name`, `last_name`, `email`, `user_category`.`name` AS `user_category` FROM `pharmacy`.`user`,`pharmacy`.`user_category` WHERE `user`.`login` = ? AND `user`.`password` = ? AND `user_category`.`user_category_id` = `user`.`user_category_id`;";
    private static final String CREATE_USER = "INSERT INTO `pharmacy`.`user`(`user_id`,`login`,`password`,`first_name`,`middle_name`,`last_name`,`email`,`user_category_id`) VALUES (default,?,?,?,?,?,?,(SELECT `user_category_id` FROM `pharmacy`.`user_category` WHERE `user_category`.`name` = UPPER(?)));";
    private static final String DELETE_USER_BY_ID = "DELETE FROM `pharmacy`.`user` WHERE `user`.`user_id` = ?;";
    private static final String FIND_LOGIN = "SELECT `user_id` FROM `pharmacy`.`user` WHERE `user`.`login` = ? ;";
    private static final String FIND_EMAIL = "SELECT `user_id` FROM `pharmacy`.`user` WHERE `user`.`email` = ? ;";

    @Override
    public List<User> findAll() throws DAOException {
        ArrayList<User> result = new ArrayList<User>();
        try{
            Connection connection = ConnectionPool.getConnection();
            try(Statement statement = connection.createStatement()){
                ResultSet resultSet = statement.executeQuery(FIND_ALL_USERS);
                while (resultSet.next()){
                    result.add(new User(
                            resultSet.getLong("user_id"),
                            resultSet.getString("login"),
                            resultSet.getString("password"),
                            resultSet.getString("first_name"),
                            resultSet.getString("middle_name"),
                            resultSet.getString("last_name"),
                            resultSet.getString("email"),
                            UserCategory.valueOf(resultSet.getString("user_category").toUpperCase())));
                }
            }finally {
                ConnectionPool.returnConnection(connection);
            }
            return result;
        }catch (SQLException | PoolingException e){
            throw new DAOException(e);
        }
    }

    @Override
    public User findElementById(Long id) throws DAOException {
        User result = null;
        try{
            Connection connection = ConnectionPool.getConnection();
            try(PreparedStatement statement = connection.prepareStatement(FIND_USER_BY_ID)){
                statement.setLong(1,id);
                ResultSet resultSet = statement.executeQuery();
                if(resultSet.next()){
                    result = (new User(
                            resultSet.getLong("user_id"),
                            resultSet.getString("login"),
                            resultSet.getString("password"),
                            resultSet.getString("first_name"),
                            resultSet.getString("middle_name"),
                            resultSet.getString("last_name"),
                            resultSet.getString("email"),
                            UserCategory.valueOf(resultSet.getString("user_category").toUpperCase())));
                }
            }finally {
                ConnectionPool.returnConnection(connection);
            }
            return result;
        }catch (SQLException | PoolingException e){
            throw new DAOException(e);
        }
    }

    @Override
    public boolean delete(User entity) throws DAOException {
        return false;
    }

    @Override
    public boolean delete(Long id) throws DAOException {
        boolean result;
        try{
            Connection connection = ConnectionPool.getConnection();
            try(PreparedStatement statement = connection.prepareStatement(DELETE_USER_BY_ID)){
                statement.setLong(1,id);
                result = statement.executeUpdate()>0;
            }finally {
                ConnectionPool.returnConnection(connection);
            }
        }catch (PoolingException | SQLException e){
            throw new DAOException(e);
        }
        return result;
    }

    @Override
    public boolean create(User entity) throws DAOException {
        boolean result;
        try{
            Connection connection = ConnectionPool.getConnection();
            try(PreparedStatement statement = connection.prepareStatement(CREATE_USER)){
                statement.setString(1,entity.getLogin());
                statement.setString(2,entity.getPassword());
                statement.setString(3,entity.getFirstName());
                statement.setString(4,entity.getMiddleName());
                statement.setString(5,entity.getLastName());
                statement.setString(6,entity.getEmail());
                statement.setString(7,entity.getUserCategory().name());
                result = statement.executeUpdate() > 0;
            }finally {
                ConnectionPool.returnConnection(connection);
            }
        }catch (PoolingException | SQLException e){
            throw new DAOException(e);
        }
        return result;
    }

    @Override
    public User update(User entity) throws DAOException {
        return null;
    }

    public User findUserByLoginAndPassword(String login, String password) throws DAOException{
        User result = null;
        try{
            Connection connection = ConnectionPool.getConnection();
            try(PreparedStatement statement = connection.prepareStatement(FIND_USER_BY_LOGIN_PASSWORD)){
                statement.setString(1,login);
                statement.setString(2,password);
                ResultSet resultSet = statement.executeQuery();
                if(resultSet.next()){
                    result = (new User(
                            resultSet.getLong("user_id"),
                            resultSet.getString("login"),
                            resultSet.getString("password"),
                            resultSet.getString("first_name"),
                            resultSet.getString("middle_name"),
                            resultSet.getString("last_name"),
                            resultSet.getString("email"),
                            UserCategory.valueOf(resultSet.getString("user_category").toUpperCase())));
                }
            }finally {
                ConnectionPool.returnConnection(connection);
            }
            return result;
        }catch (SQLException | PoolingException e){
            throw new DAOException(e);
        }
    }
    public boolean isLoginFree(String login) throws DAOException{
        try{
            Connection connection = ConnectionPool.getConnection();
            try(PreparedStatement statement = connection.prepareStatement(FIND_LOGIN)){
                statement.setString(1,login);
                return !statement.executeQuery().next();
            }finally {
                ConnectionPool.returnConnection(connection);
            }
        }catch (SQLException | PoolingException e){
            throw new DAOException(e);
        }
    }
    public boolean isEmailFree(String email) throws DAOException{
        try{
            Connection connection = ConnectionPool.getConnection();
            try(PreparedStatement statement = connection.prepareStatement(FIND_EMAIL)){
                statement.setString(1,email);
                return !statement.executeQuery().next();
            }finally {
                ConnectionPool.returnConnection(connection);
            }
        }catch (SQLException | PoolingException e){
            throw new DAOException(e);
        }
    }

}
