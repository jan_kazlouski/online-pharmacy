package com.kozlovsky.pharmacy.dao;

import com.kozlovsky.pharmacy.entity.Entity;
import com.kozlovsky.pharmacy.exception.DAOException;

import java.util.List;

public interface SearchDAO<K,T extends Entity> {
    List<T> findAll() throws DAOException;
    T findElementById(K id)throws DAOException;
}
