package com.kozlovsky.pharmacy.dao;

import com.kozlovsky.pharmacy.entity.Drug;
import com.kozlovsky.pharmacy.entity.Entity;
import com.kozlovsky.pharmacy.entity.Order;
import com.kozlovsky.pharmacy.entity.OrderStatus;
import com.kozlovsky.pharmacy.exception.DAOException;
import com.kozlovsky.pharmacy.exception.PoolingException;
import com.kozlovsky.pharmacy.pooling.ConnectionPool;
import org.apache.logging.log4j.Level;

import java.sql.PreparedStatement;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderDAO implements SearchDAO<Long,Order>, ManipulationDAO<Long,Order> {

    private static final String CREATE_ORDER = "INSERT INTO `pharmacy`.`order` (`quantity`,`order_date`,`pirce`,`user_id`,`drug_id`,`order_status_id`)VALUES (?, CURDATE(), ?,?,?,(SELECT `order_status_id` FROM `pharmacy`.`order_status` WHERE `order_status`.`name`=?));";
    private static final String FIND_ALL_ORDERS = "SELECT `order_id`, `quantity`, `order_date`, `pirce`, `user_id`, `drug_id`, `order_status`.`name` AS 'order_status' FROM `pharmacy`.`order`, `pharmacy`.`order_status` WHERE `order_status`.`order_status_id`=`order`.`order_status_id`;";
    private static final String UPDATE_STATUS = "UPDATE `pharmacy`.`order`SET `order_status_id` = (SELECT `order_status`.`order_status_id`FROM `pharmacy`.`order_status`WHERE UPPER(`order_status`.`name`)=?) WHERE `order`.`order_id` =?;";
    @Override
    public boolean delete(Order entity) throws DAOException {
        return false;
    }

    @Override
    public boolean delete(Long id) throws DAOException {
        return false;
    }

    @Override
    public boolean create(Order entity) throws DAOException {
        boolean result;
        try{
            Connection connection = ConnectionPool.getConnection();
            try(PreparedStatement statement = connection.prepareStatement(CREATE_ORDER)){
                statement.setLong(1,entity.getQuantity());
                statement.setBigDecimal(2,entity.getPrice());
                statement.setLong(3,entity.getUserId());
                statement.setLong(4,entity.getDrugId());
                statement.setString(5, OrderStatus.NEW.name());
                result = statement.executeUpdate() > 0;
            }finally {
                ConnectionPool.returnConnection(connection);
            }
        }catch (PoolingException | SQLException e){
            throw new DAOException(e);
        }
        return result;
    }

    @Override
    public Order update(Order entity) throws DAOException {
        return null;
    }

    @Override
    public List<Order> findAll() throws DAOException {
        ArrayList<Order> result = new ArrayList<>();
        try{
            Connection connection = ConnectionPool.getConnection();
            try(PreparedStatement statement = connection.prepareStatement(FIND_ALL_ORDERS)){
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()){
                    result.add(new Order(
                            resultSet.getLong("order_id"),
                            resultSet.getInt("quantity"),
                            resultSet.getDate("order_date"),
                            resultSet.getBigDecimal("pirce"),
                            resultSet.getLong("user_id"),
                            resultSet.getLong("drug_id"),
                            OrderStatus.valueOf(resultSet.getString("order_status").toUpperCase())));
                }
            }
            finally {
                ConnectionPool.returnConnection(connection);
            }
            return result;
        }catch (SQLException | PoolingException e){
            throw new DAOException(e);
        }
    }

    @Override
    public Order findElementById(Long id) throws DAOException {
        return null;
    }
    public boolean updateStatus(long orderId,OrderStatus orderStatus) throws DAOException{
        try{
            Connection connection = ConnectionPool.getConnection();
            try (PreparedStatement statement = connection.prepareStatement(UPDATE_STATUS)) {
                statement.setString(1, orderStatus.name());
                statement.setLong(2, orderId);
                return statement.executeUpdate()>0;
            } finally {
                ConnectionPool.returnConnection(connection);
            }
        }catch (PoolingException | SQLException e){
            throw new DAOException(e);
        }
    }
}
