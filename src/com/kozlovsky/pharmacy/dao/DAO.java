package com.kozlovsky.pharmacy.dao;

import com.kozlovsky.pharmacy.entity.Entity;
import com.kozlovsky.pharmacy.exception.DAOException;

import java.util.List;

public interface DAO<K,T extends Entity>   {
    List<T> findAll() throws DAOException;
    T findElementById(K id)throws DAOException;
    boolean delete(T entity)throws DAOException;
    boolean delete(K id)throws DAOException;
    boolean create(T entity)throws DAOException;
    T update(T entity)throws DAOException;
}
