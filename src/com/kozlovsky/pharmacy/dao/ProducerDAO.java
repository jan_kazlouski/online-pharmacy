package com.kozlovsky.pharmacy.dao;

import com.kozlovsky.pharmacy.entity.Producer;
import com.kozlovsky.pharmacy.exception.DAOException;
import com.kozlovsky.pharmacy.exception.PoolingException;
import com.kozlovsky.pharmacy.pooling.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class ProducerDAO implements SearchDAO<Long,Producer> {

    private static final String FIND_ALL_PRODUCERS = "SELECT `producer_id`, `name` FROM `pharmacy`.`producer`;";
    private static final String FIND_PRODUCER_BY_ID = "SELECT `producer_id`, `name` FROM `pharmacy`.`producer` WHERE `producer`.`producer_id`= ?;";
    private static final String FIND_PRODUCER_BY_DRUG_ID = "SELECT `producer`.`producer_id`, `producer`.`name` FROM `pharmacy`.`producer`, `pharmacy`.`drug` WHERE `drug`.`drug_id` = ? AND `drug`.`producer_id` = `producer`.`producer_id`;";
    private static final String FIND_PRODUCER_BY_PRODUCER_NAME = "SELECT `producer_id`, `name` FROM `pharmacy`.`producer` WHERE `producer`.`name`=?;";
    @Override
    public List<Producer> findAll() throws DAOException {
        return null;
    }

    @Override
    public Producer findElementById(Long id) throws DAOException {
        return null;
    }

    public Producer findProducerByDrugId(Long drugId) throws DAOException {
        Producer result = null;
        try{
            Connection connection = ConnectionPool.getConnection();
            try(PreparedStatement statement = connection.prepareStatement(FIND_PRODUCER_BY_DRUG_ID)){
                statement.setLong(1,drugId);
                ResultSet resultSet = statement.executeQuery();
                if(resultSet.next()){
                    result=(new Producer(
                            resultSet.getLong("producer_id"),
                            resultSet.getString("name")));
                }
            }finally {
                ConnectionPool.returnConnection(connection);
            }
            return result;
        }catch (SQLException | PoolingException e){
            throw new DAOException(e);
        }
    }
    public Producer findProducerByProducerName(String producerName) throws DAOException{
        Producer result=null;
        try{
            Connection connection = ConnectionPool.getConnection();
            try(PreparedStatement statement = connection.prepareStatement(FIND_PRODUCER_BY_PRODUCER_NAME)){
                statement.setString(1,producerName);
                ResultSet resultSet = statement.executeQuery();
                if(resultSet.next()){
                    result=(new Producer(
                            resultSet.getLong("producer_id"),
                            resultSet.getString("name")));
                }
            }finally {
                ConnectionPool.returnConnection(connection);
            }
            return result;
        }catch (SQLException | PoolingException e){
            throw new DAOException(e);
        }
    }
}
