package com.kozlovsky.pharmacy.dao;

import com.kozlovsky.pharmacy.entity.Drug;
import com.kozlovsky.pharmacy.exception.DAOException;
import com.kozlovsky.pharmacy.exception.PoolingException;
import com.kozlovsky.pharmacy.pooling.ConnectionPool;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DrugDAO implements SearchDAO<Long,Drug>, ManipulationDAO<Long,Drug>{

    private static final Logger LOGGER = LogManager.getLogger(DrugDAO.class);
    private static final String FIND_ALL_DRUGS = "SELECT `drug_id`, `name`, `description`, `require_recipe`, `price`, `initial_dose`, `maintense_dose`, `group_id`, `producer_id` FROM `pharmacy`.`drug`;";
    private static final String FIND_ALL_DRUGS_AVAILABLE_TO_CUSTOMER = "SELECT `drug_id`, `name`, `description`, `require_recipe`, `price`, `initial_dose`, `maintense_dose`, `group_id`, `producer_id` FROM `pharmacy`.`drug` WHERE  `drug`.`require_recipe`= 0 OR EXISTS(SELECT * FROM `pharmacy`.`recipe`WHERE `recipe`.`date_of_beginning` <= CURDATE() AND `recipe`.`date_of_ending`>= CURDATE() AND `drug`.`drug_id` = `recipe`.`drug_id` AND `recipe`.`client_id` = ?);";
    private static final String FIND_DRUG_BY_ID ="SELECT `drug_id`, `name`, `description`, `require_recipe`, `price`, `initial_dose`, `maintense_dose`, `group_id`, `producer_id` FROM `pharmacy`.`drug` WHERE `drug_id`=?;";
    private static final String CREATE_DRUG = "INSERT INTO `pharmacy`.`drug`(`name`,`description`,`require_recipe`,`price`,`initial_dose`,`maintense_dose`,`group_id`,`producer_id`)VALUES(?,?,?,?,?,?,?,?);";
    private static final String DELETE_DRUG_BY_ID = "DELETE FROM `pharmacy`.`drug` WHERE `drug`.`drug_id`=?;";
    private static final String UPDATE_DRUG = "UPDATE `pharmacy`.`drug` SET `name` = ?, `description` = ?, `require_recipe` = ?, `price` = ?, `initial_dose` = ?, `maintense_dose` = ?, `group_id` = ?, `producer_id` = ? WHERE `drug`.`drug_id`=?;";

    @Override
    public List<Drug> findAll() throws DAOException {
        ArrayList<Drug> result = new ArrayList<>();
        try{
            Connection connection = ConnectionPool.getConnection();
            try(PreparedStatement statement = connection.prepareStatement(FIND_ALL_DRUGS)){
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()){
                    result.add(new Drug(
                            resultSet.getLong("drug_id"),
                            resultSet.getString("name"),
                            resultSet.getString("description"),
                            resultSet.getBoolean("require_recipe"),
                            resultSet.getBigDecimal("price"),
                            resultSet.getString("initial_dose"),
                            resultSet.getString("maintense_dose"),
                            resultSet.getLong("group_id"),
                            resultSet.getLong("producer_id")));
                }
            }
            finally {
                ConnectionPool.returnConnection(connection);
            }
            return result;
        }catch (SQLException | PoolingException e){
            throw new DAOException(e);
        }
    }

    @Override
    public Drug findElementById(Long id) throws DAOException {
        Drug result = null;
        try{
            Connection connection = ConnectionPool.getConnection();
            try(PreparedStatement statement = connection.prepareStatement(FIND_DRUG_BY_ID)){
                statement.setLong(1,id);
                ResultSet resultSet = statement.executeQuery();
                if(resultSet.next()){
                    result=(new Drug(
                            resultSet.getLong("drug_id"),
                            resultSet.getString("name"),
                            resultSet.getString("description"),
                            resultSet.getBoolean("require_recipe"),
                            resultSet.getBigDecimal("price"),
                            resultSet.getString("initial_dose"),
                            resultSet.getString("maintense_dose"),
                            resultSet.getLong("group_id"),
                            resultSet.getLong("producer_id")));
                }
            }finally {
                ConnectionPool.returnConnection(connection);
            }
            return result;
        }catch (SQLException | PoolingException e){
            throw new DAOException(e);
        }
    }

    @Override
    public boolean delete(Drug entity) throws DAOException {
        return false;
    }

    @Override
    public boolean delete(Long id) throws DAOException {
        boolean result;
        try{
            Connection connection = ConnectionPool.getConnection();
            try(PreparedStatement statement = connection.prepareStatement(DELETE_DRUG_BY_ID)){
                statement.setLong(1,id);
                result = statement.executeUpdate()>0;
            }finally {
                ConnectionPool.returnConnection(connection);
            }
        }catch (PoolingException | SQLException e){
            throw new DAOException(e);
        }
        return result;
    }

    @Override
    public boolean create(Drug entity) throws DAOException {
        boolean result;
        try{
            Connection connection = ConnectionPool.getConnection();
            try(PreparedStatement statement = connection.prepareStatement(CREATE_DRUG)){
                statement.setString(1,entity.getName());
                statement.setString(2,entity.getDescription());
                statement.setBoolean(3,entity.getRequireRecipe());
                statement.setBigDecimal(4,entity.getPrice());
                statement.setString(5,entity.getInitialDose());
                statement.setString(6,entity.getMaintenseDose());
                statement.setLong(7,entity.getGroupId());
                statement.setLong(8,entity.getProducerId());
                result = statement.executeUpdate() > 0;
            }finally {
                ConnectionPool.returnConnection(connection);
            }
        }catch (PoolingException | SQLException e){
            throw new DAOException(e);
        }
        return result;
    }

    @Override
    public Drug update(Drug entity) throws DAOException {
        try{
            Connection connection = ConnectionPool.getConnection();
            try (PreparedStatement statement = connection.prepareStatement(UPDATE_DRUG)) {
                statement.setString(1, entity.getName());
                statement.setString(2, entity.getDescription());
                statement.setBoolean(3,entity.getRequireRecipe());
                statement.setBigDecimal(4,entity.getPrice());
                statement.setString(5,entity.getInitialDose());
                statement.setString(6,entity.getMaintenseDose());
                statement.setLong(7,entity.getGroupId());
                statement.setLong(8,entity.getProducerId());
                statement.setLong(9,entity.getId());
                if (statement.executeUpdate() > 0) {
                    LOGGER.log(Level.INFO,entity + " updated");
                }else{
                    throw new DAOException("Can't update " + entity);
                }
            } finally {
                ConnectionPool.returnConnection(connection);
            }
        }catch (PoolingException | SQLException e){
            throw new DAOException(e);
        }
        return entity;
    }


    public List<Drug> findAllDrugsAvailableToCustomer(Long customerId) throws DAOException{
        ArrayList<Drug> result = new ArrayList<>();
        try{
            Connection connection = ConnectionPool.getConnection();
            try(PreparedStatement statement = connection.prepareStatement(FIND_ALL_DRUGS_AVAILABLE_TO_CUSTOMER)){
                statement.setLong(1,customerId);
                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()){
                    result.add(new Drug(
                            resultSet.getLong("drug_id"),
                            resultSet.getString("name"),
                            resultSet.getString("description"),
                            resultSet.getBoolean("require_recipe"),
                            resultSet.getBigDecimal("price"),
                            resultSet.getString("initial_dose"),
                            resultSet.getString("maintense_dose"),
                            resultSet.getLong("group_id"),
                            resultSet.getLong("producer_id")));
                }
            }
            finally {
                ConnectionPool.returnConnection(connection);
            }
            return result;
        }catch (SQLException | PoolingException e){
            throw new DAOException(e);
        }
    }
}
