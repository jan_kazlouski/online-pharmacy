package com.kozlovsky.pharmacy.dao;

import com.kozlovsky.pharmacy.entity.Recipe;
import com.kozlovsky.pharmacy.exception.DAOException;
import com.kozlovsky.pharmacy.exception.PoolingException;
import com.kozlovsky.pharmacy.pooling.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class RecipeDAO implements ManipulationDAO<Long,Recipe>{
    private static final String CREATE_RECIPE = "INSERT INTO `pharmacy`.`recipe`(`date_of_beginning`,`date_of_ending`,`quantity`,`client_id`,`doctor_id`,`drug_id`)VALUES(?,?,?,?,?,?);";
    @Override
    public boolean delete(Recipe entity) throws DAOException {
        return false;
    }

    @Override
    public boolean delete(Long id) throws DAOException {
        return false;
    }

    @Override
    public boolean create(Recipe entity) throws DAOException {
        boolean result;
        try{
            Connection connection = ConnectionPool.getConnection();
            try(PreparedStatement statement = connection.prepareStatement(CREATE_RECIPE)){
                statement.setDate(1,entity.getDateOfBeginning());
                statement.setDate(2,entity.getDateOfEnding());
                statement.setInt(3,entity.getQuantity());
                statement.setLong(4,entity.getClientId());
                statement.setLong(5,entity.getDoctorId());
                statement.setLong(6,entity.getDrugId());
                result = statement.executeUpdate() > 0;
            }finally {
                ConnectionPool.returnConnection(connection);
            }
        }catch (PoolingException | SQLException e){
            throw new DAOException(e);
        }
        return result;
    }

    @Override
    public Recipe update(Recipe entity) throws DAOException {
        return null;
    }
}
