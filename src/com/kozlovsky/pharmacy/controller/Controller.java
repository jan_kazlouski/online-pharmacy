package com.kozlovsky.pharmacy.controller;

import com.kozlovsky.pharmacy.command.ChangeLanguageCommand;
import com.kozlovsky.pharmacy.command.Command;
import com.kozlovsky.pharmacy.command.factory.ActionFactory;
import com.kozlovsky.pharmacy.exception.PoolingException;
import com.kozlovsky.pharmacy.memento.Caretaker;
import com.kozlovsky.pharmacy.memento.MementoRequest;
import com.kozlovsky.pharmacy.pooling.ConnectionPool;
import com.kozlovsky.pharmacy.resource.ConfigurationManager;
import com.kozlovsky.pharmacy.resource.MessageManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/controller")
public class Controller extends HttpServlet {

    private static final String MEMENTO = "memento";
    private static final String NULL_PAGE = "nullPage";
    private static Logger LOGGER = LogManager.getLogger(Controller.class);
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req,resp);
    }

    @Override
    public void init() throws ServletException {
    }

    @Override
    public void destroy() {
        try {
            ConnectionPool.takePoolDown();
        }catch (PoolingException e){
            LOGGER.log(Level.ERROR,e);
        }
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ActionFactory factory = new ActionFactory();
        Command command = factory.defineCommand(request);
        String page = command.execute(request);

        if (page != null) {
            MementoRequest memento = (MementoRequest) request.getSession().getAttribute("memento");
            if (memento == null) {
                memento = new MementoRequest();
            }
            Caretaker caretaker = new Caretaker(memento);
            if (command.getClass() != ChangeLanguageCommand.class) {
                caretaker.extract(request);
                request.getSession().setAttribute(MEMENTO, memento);
            } else {
                caretaker.fill(request);
            }
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
            dispatcher.forward(request, response);
        } else {
            LOGGER.log(Level.ERROR,"Page is null");
            page = ConfigurationManager.getProperty("path.page.index");
            request.getSession().setAttribute(NULL_PAGE, MessageManager.getProperty("message.nullpage"));
            response.sendRedirect(request.getContextPath() + page);
        }
    }
}
