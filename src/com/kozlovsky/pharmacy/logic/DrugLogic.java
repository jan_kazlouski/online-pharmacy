package com.kozlovsky.pharmacy.logic;

import com.kozlovsky.pharmacy.dao.DrugDAO;
import com.kozlovsky.pharmacy.dao.GroupDAO;
import com.kozlovsky.pharmacy.dao.ProducerDAO;
import com.kozlovsky.pharmacy.entity.Drug;
import com.kozlovsky.pharmacy.entity.Group;
import com.kozlovsky.pharmacy.entity.Producer;
import com.kozlovsky.pharmacy.exception.DAOException;
import com.kozlovsky.pharmacy.exception.LogicException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;

public class DrugLogic{

    private static final Logger LOGGER = LogManager.getLogger(DrugLogic.class);
    public static ArrayList<Drug> takeAllDrugsAvailableToClient(long clientId) throws LogicException {
        DrugDAO drugDAO = new DrugDAO();
        try {
            ArrayList<Drug> result = (ArrayList<Drug>)drugDAO.findAllDrugsAvailableToCustomer(clientId);
            return result;
        }catch (DAOException e){
            throw new LogicException(e);
        }
    }
    public static Drug takeDrug(long drugId) throws LogicException{
        DrugDAO drugDAO = new DrugDAO();
        try {
            Drug result = drugDAO.findElementById(drugId);
            return result;
        }catch (DAOException e){
            throw new LogicException(e);
        }
    }
    public static ArrayList<Drug> takeAllDrugs() throws LogicException{
        DrugDAO drugDAO = new DrugDAO();
        try {
            ArrayList<Drug> result = (ArrayList<Drug>)drugDAO.findAll();
            return result;
        }catch (DAOException e){
            throw new LogicException(e);
        }
    }

    public static boolean addDrug(String name, String description, boolean requireRecipe, BigDecimal price, String initialDose, String maintenseDose, String groupName, String producerName) throws LogicException{
        DrugDAO drugDAO = new DrugDAO();
        GroupDAO groupDAO = new GroupDAO();
        ProducerDAO producerDAO = new ProducerDAO();
        try {
            Group group = groupDAO.findGroupByGroupName(groupName);
            Producer producer = producerDAO.findProducerByProducerName(producerName);
            Drug drug = new Drug(0,name,description,requireRecipe,price,initialDose,maintenseDose,group.getId(),producer.getId());
            return drugDAO.create(drug);
        }catch (DAOException e){
            throw new LogicException(e);
        }
    }

    public static boolean deleteDrug(long drugId) throws LogicException{
        DrugDAO drugDAO = new DrugDAO();
        try {
            return drugDAO.delete(drugId);
        }catch (DAOException e){
            throw new LogicException(e);
        }
    }

    public static Drug editDrug(long drugId, String name, String description, boolean requireRecipe, BigDecimal price, String initialDose, String maintenseDose, String groupName, String producerName) throws LogicException{
        DrugDAO drugDAO = new DrugDAO();
        GroupDAO groupDAO = new GroupDAO();
        ProducerDAO producerDAO = new ProducerDAO();
        try{
            if(drugDAO.findElementById(drugId)==null){
                throw new LogicException();
            }
            Group newGroup = groupDAO.findGroupByGroupName(groupName);
            Producer newProducer = producerDAO.findProducerByProducerName(producerName);
            Drug newDrug = new Drug(drugId,name,description,requireRecipe,price,initialDose,maintenseDose,newGroup.getId(),newProducer.getId());
            return drugDAO.update(newDrug);
        }catch (DAOException e){
            throw new LogicException(e);
        }
    }
}
