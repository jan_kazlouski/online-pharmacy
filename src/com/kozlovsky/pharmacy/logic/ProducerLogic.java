package com.kozlovsky.pharmacy.logic;

import com.kozlovsky.pharmacy.dao.ProducerDAO;
import com.kozlovsky.pharmacy.entity.Producer;
import com.kozlovsky.pharmacy.exception.DAOException;
import com.kozlovsky.pharmacy.exception.LogicException;

public class ProducerLogic {
    public static Producer takeProducer(long drugId) throws LogicException {
        ProducerDAO producerDAO = new ProducerDAO();
        try {
            Producer result = producerDAO.findProducerByDrugId(drugId);
            return result;
        }catch (DAOException e){
            throw new LogicException(e);
        }
    }
}
