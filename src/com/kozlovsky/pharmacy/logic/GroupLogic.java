package com.kozlovsky.pharmacy.logic;

import com.kozlovsky.pharmacy.dao.GroupDAO;
import com.kozlovsky.pharmacy.entity.Group;
import com.kozlovsky.pharmacy.exception.DAOException;
import com.kozlovsky.pharmacy.exception.LogicException;

public class GroupLogic {
    public static Group takeGroup(long drugId) throws LogicException {
        GroupDAO groupDAO = new GroupDAO();
        try {
            Group result = groupDAO.findGroupByDrugId(drugId);
            return result;
        }catch (DAOException e){
            throw new LogicException(e);
        }
    }
}
