package com.kozlovsky.pharmacy.logic;

import com.kozlovsky.pharmacy.dao.UserDAO;
import com.kozlovsky.pharmacy.entity.User;
import com.kozlovsky.pharmacy.entity.UserCategory;
import com.kozlovsky.pharmacy.exception.LogicException;
import com.kozlovsky.pharmacy.exception.DAOException;
import com.kozlovsky.pharmacy.logic.encryption.MD5;
import com.kozlovsky.pharmacy.logic.validation.RegexValidator;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class UserLogic {
    private static final String REGEX_LOGIN = "(\\w|\\d){1,30}";
    private static final String REGEX_PASSWORD = "(\\w|\\d){3,20}";
    private static final String REGEX_EMAIL_FORMAT = "(\\w|\\d)+@\\w+\\.\\w+";
    private static final String REGEX_EMAIL_LENGTH = ".{5,40}";
    private static final String REGEX_NAME = "([A-Z][A-Za-z]*([ -][A-Z][A-Za-z]*)*)|([А-Я][А-Яа-я]*([ -][А-Я][А-Яа-я]*)*)";
    private static final String REGEX_NAME_LENGTH = ".{1,30}";
    private static final String REGEX_NAME_CAPITAL = "([A-ZА-Я].*)|(.+[ -'][A-ZА-Я].*)";

    public static User checkLogin(String login,String password)throws LogicException {
        if (!validateLogin(login) || !validatePassword(password)) {
            return null;
        }
        try{
            UserDAO dao = new UserDAO();
            String encryptedPassword = MD5.encrypt(password);
            return dao.findUserByLoginAndPassword(login, encryptedPassword);
        }catch (DAOException e){
            throw new LogicException(e);
        }
    }
    public static RegistrationResult register(String login, String password,String repeatPassword, String firstName, String middleName, String lastName, String email, User user) throws LogicException{
        RegistrationResult result = null;
        if(!validateLogin(login) || !validatePassword(password)){
            result = RegistrationResult.LOGIN_PASS_INCORRECT;
        } else if (!validateEmail(email)) {
            result = RegistrationResult.EMAIL_INCORRECT;
        } else if (!password.equals(repeatPassword)) {
            result = RegistrationResult.PASS_NOT_MATCH;
        } else if (!validateName(firstName)||!validateName(middleName)||!validateName(lastName)) {
            result = RegistrationResult.NAME_INCORRECT;
        } else {
            try{
                UserDAO userDAO = new UserDAO();
                if(userDAO.isLoginFree(login)){
                    if(userDAO.isEmailFree(email)){
                        String encryptedPassword = MD5.encrypt(password);
                        user.setLogin(login);
                        user.setPassword(encryptedPassword);
                        user.setEmail(email);
                        user.setFirstName(firstName);
                        user.setMiddleName(middleName);
                        user.setLastName(lastName);
                        user.setUserCategory(UserCategory.CLIENT);
                        if(userDAO.create(user)){
                            user.setId(userDAO.findUserByLoginAndPassword(login,encryptedPassword).getId());
                            result = RegistrationResult.ALL_RIGHT;
                        } else {
                            throw new LogicException("user cannot be created");
                        }
                    }else {
                        result = RegistrationResult.EMAIL_NOT_UNIQUE;
                    }
                } else {
                    result = RegistrationResult.LOGIN_NOT_UNIQUE;
                }
            }catch (DAOException e){
                throw new LogicException();
            }
        }
        return result;
    }
    private static boolean validateLogin(String login){
        return RegexValidator.validate(login,REGEX_LOGIN);
    }
    private static boolean validatePassword(String password){
        return RegexValidator.validate(password,REGEX_PASSWORD);
    }
    private static boolean validateEmail(String email){
        return RegexValidator.validate(email,REGEX_EMAIL_FORMAT,REGEX_EMAIL_LENGTH);
    }
    private static boolean validateName(String name){
        return RegexValidator.validate(name,REGEX_NAME,REGEX_NAME_CAPITAL,REGEX_NAME_LENGTH);
    }
    public static List<User> takeAllUsersExceptAdmins() throws LogicException {
        UserDAO userDAO = new UserDAO();
        try {
            ArrayList<User> result = (ArrayList<User>)userDAO.findAll();
            result.removeIf(x->x.getUserCategory()==UserCategory.ADMIN);
            return result;
        }catch (DAOException e){
            throw new LogicException(e);
        }
    }
    public static User takeUser(long userId) throws LogicException{
        UserDAO userDAO = new UserDAO();
        try {
            User result = userDAO.findElementById(userId);
            return result;
        }catch (DAOException e){
            throw new LogicException(e);
        }
    }
    public static boolean deleteUser(long adminId, long userId) throws LogicException{
        boolean result;
        UserDAO userDAO = new UserDAO();
        try {
            User admin = userDAO.findElementById(adminId);
            User user = userDAO.findElementById(userId);;
            if(admin.getUserCategory()!=UserCategory.ADMIN
                    || user.getUserCategory()==UserCategory.ADMIN){
                return false;
            }
            result = userDAO.delete(userId);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
        return result;
    }
}
