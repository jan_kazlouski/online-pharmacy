package com.kozlovsky.pharmacy.logic.validation;

import com.kozlovsky.pharmacy.dao.DrugDAO;
import com.kozlovsky.pharmacy.dao.UserDAO;
import com.kozlovsky.pharmacy.entity.Drug;
import com.kozlovsky.pharmacy.entity.Order;
import com.kozlovsky.pharmacy.entity.User;
import com.kozlovsky.pharmacy.entity.UserCategory;
import com.kozlovsky.pharmacy.exception.DAOException;
import com.kozlovsky.pharmacy.exception.LogicException;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.Instant;

public class OrderValidator {
    public static boolean validate(Order order)throws LogicException {
        UserDAO userDAO = new UserDAO();
        DrugDAO drugDAO = new DrugDAO();
        try{
            User client = userDAO.findElementById(order.getUserId());
            Drug drug = drugDAO.findElementById(order.getDrugId());

            return !(client==null || drug==null || client.getUserCategory() != UserCategory.CLIENT || order.getQuantity()<=0);
        }catch (DAOException e){
            throw new LogicException(e);
        }
    }

}
