package com.kozlovsky.pharmacy.logic.validation;

import com.kozlovsky.pharmacy.dao.DrugDAO;
import com.kozlovsky.pharmacy.dao.UserDAO;
import com.kozlovsky.pharmacy.entity.Drug;
import com.kozlovsky.pharmacy.entity.Recipe;
import com.kozlovsky.pharmacy.entity.User;
import com.kozlovsky.pharmacy.entity.UserCategory;
import com.kozlovsky.pharmacy.exception.DAOException;
import com.kozlovsky.pharmacy.exception.LogicException;

import java.time.Instant;
import java.util.Date;

public class RecipeValidator {
    public static boolean validate(Recipe recipe)throws LogicException{
        UserDAO userDAO = new UserDAO();
        DrugDAO drugDAO = new DrugDAO();
        try{
            User client = userDAO.findElementById(recipe.getClientId());
            User doctor = userDAO.findElementById(recipe.getDoctorId());
            Drug drug = drugDAO.findElementById(recipe.getDrugId());
            return !(recipe.getDateOfBeginning().after(recipe.getDateOfEnding()) ||
                    recipe.getDateOfEnding().before(Date.from(Instant.now()))||
                    client==null || doctor==null || drug==null ||
                    client.getUserCategory() != UserCategory.CLIENT ||
                    doctor.getUserCategory() != UserCategory.DOCTOR ||
                    !drug.getRequireRecipe() || recipe.getQuantity()<=0);
        }catch (DAOException e){
            throw new LogicException(e);
        }
    }
}
