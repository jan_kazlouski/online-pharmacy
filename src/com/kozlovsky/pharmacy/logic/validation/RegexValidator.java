package com.kozlovsky.pharmacy.logic.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexValidator {

    public static boolean validate(String target, String... regularExpressions) {
        boolean valid = true;
        Pattern pattern;
        Matcher matcher;
        for (String regExp : regularExpressions) {
            pattern = Pattern.compile(regExp);
            matcher = pattern.matcher(target);
            valid = valid && matcher.matches();
        }
        return valid;
    }
}
