package com.kozlovsky.pharmacy.logic;

import com.kozlovsky.pharmacy.dao.RecipeDAO;
import com.kozlovsky.pharmacy.entity.Recipe;
import com.kozlovsky.pharmacy.exception.DAOException;
import com.kozlovsky.pharmacy.exception.LogicException;
import com.kozlovsky.pharmacy.logic.validation.RecipeValidator;

import java.sql.Date;

public class RecipeLogic {
    public static boolean addRecipe(Date dateOfBeginning, Date dateOfEnding, int quantity, long clientId, long doctorId, long drugId) throws LogicException {
        boolean result = false;
        RecipeDAO recipeDAO = new RecipeDAO();
        Recipe recipe = new Recipe();
        recipe.setDateOfBeginning(dateOfBeginning);
        recipe.setDateOfEnding(dateOfEnding);
        recipe.setQuantity(quantity);
        recipe.setClientId(clientId);
        recipe.setDoctorId(doctorId);
        recipe.setDrugId(drugId);
        try {
            return RecipeValidator.validate(recipe) && recipeDAO.create(recipe);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }
}
