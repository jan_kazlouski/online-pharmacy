package com.kozlovsky.pharmacy.logic;

import com.kozlovsky.pharmacy.dao.DrugDAO;
import com.kozlovsky.pharmacy.dao.OrderDAO;
import com.kozlovsky.pharmacy.dao.RecipeDAO;
import com.kozlovsky.pharmacy.entity.Drug;
import com.kozlovsky.pharmacy.entity.Order;
import com.kozlovsky.pharmacy.entity.OrderStatus;
import com.kozlovsky.pharmacy.exception.DAOException;
import com.kozlovsky.pharmacy.exception.LogicException;
import com.kozlovsky.pharmacy.logic.validation.OrderValidator;
import com.mysql.jdbc.log.Log;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;

public class OrderLogic {
    public static boolean addOrder(int quantity, long clientId, long drugId) throws LogicException {
        DrugDAO drugDAO = new DrugDAO();
        OrderDAO orderDAO = new OrderDAO();
        try {
            Drug drug = drugDAO.findElementById(drugId);
            Order order = new Order();
            order.setPrice(drug.getPrice().multiply(new BigDecimal(quantity)));
            order.setUserId(clientId);
            order.setDrugId(drugId);
            order.setQuantity(quantity);
            OrderValidator.validate(order);
            return orderDAO.create(order);
        }catch (DAOException e){
            throw new LogicException(e);
        }
    }
    public static ArrayList<Order> takeAllOrders() throws LogicException{
        OrderDAO orderDAO = new OrderDAO();
        try {
            ArrayList<Order> result = (ArrayList<Order>)orderDAO.findAll();
            return result;
        }catch (DAOException e){
            throw new LogicException(e);
        }
    }
    public static ArrayList<Order> takeActiveOrders() throws LogicException{
        OrderDAO orderDAO = new OrderDAO();
        try {
            ArrayList<Order> result = (ArrayList<Order>)orderDAO.findAll();
            result.removeIf(x->x.getOrderStatus()!=OrderStatus.IN_PROCESS);
            return result;
        }catch (DAOException e){
            throw new LogicException(e);
        }
    }
    public static boolean finishOrder(long orderId)throws LogicException{
        OrderDAO orderDAO = new OrderDAO();
        try {
            return orderDAO.updateStatus(orderId,OrderStatus.PROCESSED);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }
    public static boolean cancelOrder(long orderId)throws LogicException{
        OrderDAO orderDAO = new OrderDAO();
        try {
            return orderDAO.updateStatus(orderId,OrderStatus.REJECTED);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }
    public static boolean processOrder(long orderId)throws LogicException{
        OrderDAO orderDAO = new OrderDAO();
        try {
            return orderDAO.updateStatus(orderId,OrderStatus.IN_PROCESS);
        } catch (DAOException e) {
            throw new LogicException(e);
        }
    }
}
