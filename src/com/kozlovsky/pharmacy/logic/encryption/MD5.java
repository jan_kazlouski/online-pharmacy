package com.kozlovsky.pharmacy.logic.encryption;

import com.kozlovsky.pharmacy.exception.LogicException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5 {
    private static Logger LOGGER = LogManager.getLogger(MD5.class);

    private static final String MD5 = "MD5";

    public static String encrypt(String word) throws LogicException {
        if(word==null){
            throw new LogicException();
        }
        MessageDigest messageDigest;
        byte[] digest = new byte[0];
        try {
            messageDigest = MessageDigest.getInstance(MD5);
            messageDigest.reset();
            messageDigest.update(word.getBytes());
            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Algorithm wasn't recognized.", e);
        }
        BigInteger bigIntPass = new BigInteger(1, digest);
        return bigIntPass.toString(16);
    }
}
