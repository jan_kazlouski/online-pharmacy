package com.kozlovsky.pharmacy.logic;

public enum  RegistrationResult {
    ALL_RIGHT,
    LOGIN_PASS_INCORRECT,
    EMAIL_INCORRECT,
    PASS_NOT_MATCH,
    LOGIN_NOT_UNIQUE,
    EMAIL_NOT_UNIQUE,
    NAME_INCORRECT,
}
