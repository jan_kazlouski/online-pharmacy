package com.kozlovsky.pharmacy.filter;

import com.kozlovsky.pharmacy.command.factory.ActionFactory;
import com.kozlovsky.pharmacy.command.Command;
import com.kozlovsky.pharmacy.entity.UserCategory;
import com.kozlovsky.pharmacy.resource.ConfigurationManager;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

@WebFilter(dispatcherTypes = {
        DispatcherType.FORWARD,
        DispatcherType.INCLUDE,
        DispatcherType.REQUEST
},urlPatterns = {"/controller"},
        servletNames = {"controller"})
public class CommandValidationFilter implements Filter {

    @Override
    public void init(FilterConfig fConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession httpSession = httpRequest.getSession();

        Command command = new ActionFactory().defineCommand(httpRequest);
        if(command.checkAccess((UserCategory) httpSession.getAttribute("category"))){
            chain.doFilter(request, response);
        }else{
            httpResponse.sendRedirect(httpRequest.getContextPath() + ConfigurationManager.getProperty("path.page.error"));
        }
    }

    @Override
    public void destroy() {
    }
}
