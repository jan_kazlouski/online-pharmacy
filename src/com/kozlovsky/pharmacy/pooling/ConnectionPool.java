package com.kozlovsky.pharmacy.pooling;

import com.kozlovsky.pharmacy.exception.PoolingException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPool {
    private static final String RESOURCE = "resources.database";
    private static final String URL = "url";
    private static final String USER = "user";
    private static final String PASSWORD = "password";
    private static final String AUTORECONNECT = "autoReconnect";
    private static final String CHARENCODING = "characterEncoding";
    private static final String USEUNICODE = "useUnicode";
    private static final String POOLSIZE = "poolsize";
    private static final String USESSL = "useSSL";

    private static ConnectionPool instance;
    private static AtomicBoolean instanceCreated = new AtomicBoolean(false);
    private static ReentrantLock lock = new ReentrantLock();
    private AtomicBoolean isUp;
    private AtomicBoolean isClosing;
    private ArrayDeque<Connection> freeConnections;
    private ArrayDeque<Connection> activeConnections;

    private ConnectionPool(){
        freeConnections = new ArrayDeque<>();
        activeConnections = new ArrayDeque<>();
        isClosing = new AtomicBoolean(false);
        isUp = new AtomicBoolean(false);
    }

    private static ConnectionPool getInstance(){
        if(!instanceCreated.get()){
            lock.lock();
            try{
                if(!instanceCreated.get()){
                    instance = new ConnectionPool();
                    instanceCreated.set(true);
                }
            }finally {
                lock.unlock();
            }
        }
        return instance;
    }

    public static Connection getConnection() throws PoolingException{
        Connection connection = null;
        if(!getInstance().isUp.get()){
            lock.lock();
            if(!getInstance().isUp.get()){
                setPoolUp();
            }
            lock.unlock();
        }
        if(!getInstance().isClosing.get()){
            lock.lock();
            if(!getInstance().freeConnections.isEmpty()){
                getInstance().activeConnections.addLast(getInstance().freeConnections.removeLast());
                connection = getInstance().activeConnections.getLast();
            }
            lock.unlock();
        }
        return connection;
    }

    public static void returnConnection(Connection connection) throws PoolingException{
        lock.lock();
        try {
            if(getInstance().activeConnections.contains(connection)){
                getInstance().freeConnections.addLast(connection);
                getInstance().activeConnections.remove(connection);

            }else {
                throw new PoolingException("Cannot return connection to pool");
            }
        } finally {
            lock.unlock();
        }
    }

    private static void setPoolUp() throws PoolingException{
        try{
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            ResourceBundle resource = ResourceBundle.getBundle(RESOURCE);
            String url = resource.getString(URL);
            String user = resource.getString(USER);
            String pass = resource.getString(PASSWORD);
            String autoReconnect = resource.getString(AUTORECONNECT);
            String charEncoding = resource.getString(CHARENCODING);
            String useUnicode = resource.getString(USEUNICODE);
            String useSSL = resource.getString(USESSL);
            int poolSize = Integer.parseInt(resource.getString(POOLSIZE));
            Properties properties = new Properties();
            properties.put(USER, user);
            properties.put(PASSWORD, pass);
            properties.put(AUTORECONNECT, autoReconnect);
            properties.put(CHARENCODING, charEncoding);
            properties.put(USEUNICODE, useUnicode);
            properties.put(USESSL, useSSL);
            for(int i = 0; i< poolSize; i++) {
                getInstance().freeConnections.add(DriverManager.getConnection(url,properties));
            }
            getInstance().isUp.set(true);
        }catch (SQLException e){
            throw new PoolingException(e);
        }
    }

    public static void takePoolDown() throws PoolingException{
        lock.lock();
        getInstance().isClosing.set(true);
        try{
            for(Connection connection:getInstance().freeConnections){
                if(connection==null){
                    TimeUnit.SECONDS.sleep(5);
                }
                if(connection != null){
                    connection.close();
                }
                else{
                    throw new PoolingException();
                }
            }
            deregisterPoolDriver();
        }catch (SQLException | InterruptedException e){
            throw new PoolingException(e);
        }
        getInstance().isUp.set(false);
        lock.unlock();
    }

    private static void deregisterPoolDriver() throws SQLException{
            DriverManager.deregisterDriver(new com.mysql.jdbc.Driver());
    }
    public static boolean isEmpty(){
        return getInstance().freeConnections.isEmpty()||!getInstance().isUp.get();
    }
}
