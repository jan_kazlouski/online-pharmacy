package test.com.kozlovsky.pharmacy.dao;

import com.kozlovsky.pharmacy.dao.DrugDAO;
import com.kozlovsky.pharmacy.entity.Drug;
import com.kozlovsky.pharmacy.exception.DAOException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;

public class DrugDAOTest {
    @Test
    public void findAllDrugsAvailableToCustomerTest() throws DAOException {
        DrugDAO drugDAO = new DrugDAO();
        ArrayList<Drug> actual = (ArrayList<Drug>)drugDAO.findAllDrugsAvailableToCustomer(3L);
        Assert.assertEquals(actual.size(),6);
    }

}
