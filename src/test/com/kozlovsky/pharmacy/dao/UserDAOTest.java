package test.com.kozlovsky.pharmacy.dao;

import com.kozlovsky.pharmacy.dao.UserDAO;
import com.kozlovsky.pharmacy.entity.User;
import com.kozlovsky.pharmacy.entity.UserCategory;
import com.kozlovsky.pharmacy.exception.DAOException;
import com.kozlovsky.pharmacy.exception.PoolingException;
import com.kozlovsky.pharmacy.pooling.ConnectionPool;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class UserDAOTest {
    @Test
    public void findElementByLoginAndPasswordTest() throws DAOException{
        UserDAO userDAO = new UserDAO();
        User actual = userDAO.findUserByLoginAndPassword("root","63a9f0ea7bb98050796b649e85481845");
        User expected = new User(1,"root","63a9f0ea7bb98050796b649e85481845","Jack","Arthur","Nicholson","root@gmail.com", UserCategory.ADMIN);
        Assert.assertEquals(actual,expected);
    }
    @Test
    public void findElementByIdTest() throws DAOException{
        UserDAO userDAO = new UserDAO();
        User actual = userDAO.findElementById(1L);
        User expected = new User(1,"root","63a9f0ea7bb98050796b649e85481845","Jack","Arthur","Nicholson","root@gmail.com",UserCategory.ADMIN);
        Assert.assertEquals(actual,expected);
    }
    @AfterClass
    public void destroy()throws PoolingException{
        ConnectionPool.takePoolDown();
    }
}
