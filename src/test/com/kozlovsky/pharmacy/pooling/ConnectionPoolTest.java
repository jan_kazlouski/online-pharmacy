package test.com.kozlovsky.pharmacy.pooling;

import com.kozlovsky.pharmacy.exception.PoolingException;
import com.kozlovsky.pharmacy.pooling.ConnectionPool;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import java.sql.Connection;

public class ConnectionPoolTest {
    @Test
    public void setPoolUpTest() throws PoolingException{
        for (int i = 0; i<10;i++){
            Connection[] connections = new Connection[5];
            for(int j=0;j<5;j++){
                connections[j] = ConnectionPool.getConnection();
            }
            for(int j=0;j<5;j++){
                ConnectionPool.returnConnection(connections[j]);
            }
        }
    }
    @AfterClass
    public void destroyPool()throws PoolingException{
        ConnectionPool.takePoolDown();
    }
}
