package test.com.kozlovsky.pharmacy.logic.encryption;

import com.kozlovsky.pharmacy.exception.LogicException;
import com.kozlovsky.pharmacy.logic.encryption.MD5;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MD5Test {
    @Test
    public void encryptTestOK() throws LogicException {
        String actual = MD5.encrypt("root");
        String expected = "63a9f0ea7bb98050796b649e85481845";
        Assert.assertEquals(actual,expected);
    }
    @Test (expectedExceptions = LogicException.class)
    public void encryptTestNullArgument() throws LogicException {
        String actual = MD5.encrypt(null);
    }
}
