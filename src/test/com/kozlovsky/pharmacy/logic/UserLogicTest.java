package test.com.kozlovsky.pharmacy.logic;

import com.kozlovsky.pharmacy.entity.User;
import com.kozlovsky.pharmacy.exception.LogicException;
import com.kozlovsky.pharmacy.logic.RegistrationResult;
import com.kozlovsky.pharmacy.logic.UserLogic;
import org.testng.Assert;
import org.testng.annotations.Test;

public class UserLogicTest {
    @Test
    public void registerTestOK() throws LogicException {
        Assert.assertEquals(UserLogic.register("test","test","test","Test","Test","Test","test@gmail.com",new User()), RegistrationResult.ALL_RIGHT);
    }
    @Test
    public void registerTestLoginPassIncorrect() throws LogicException {
        Assert.assertEquals(UserLogic.register("test1","t","t","Test","Test","Test","test1@gmail.com",new User()), RegistrationResult.LOGIN_PASS_INCORRECT);
    }
    @Test
    public void registerTestEmailIncorrect() throws LogicException {
        Assert.assertEquals(UserLogic.register("test2","test","test","Test","Test","Test","te",new User()), RegistrationResult.EMAIL_INCORRECT);
    }
    @Test
    public void registerTestPassNotMatch() throws LogicException {
        Assert.assertEquals(UserLogic.register("test3","test","wrong","Test","Test","Test","test2@gmail.com",new User()), RegistrationResult.PASS_NOT_MATCH);
    }
    @Test
    public void registerTestLoginNotUnique() throws LogicException {
        Assert.assertEquals(UserLogic.register("client1","test","test","Test","Test","Test","test3@gmail.com",new User()), RegistrationResult.LOGIN_NOT_UNIQUE);
    }
    @Test
    public void registerTestEmailNotUnique() throws LogicException {
        Assert.assertEquals(UserLogic.register("test4","test","test","Test","Test","Test","client1@gmail.com",new User()), RegistrationResult.EMAIL_NOT_UNIQUE);
    }
    @Test
    public void registerTestNameIncorrect() throws LogicException {
        Assert.assertEquals(UserLogic.register("test5","test","test","incorrect","Test","Test","test4@gmail.com",new User()), RegistrationResult.NAME_INCORRECT);
    }
}
