<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="customtags" prefix="ctg"%>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="resources.pagecontent" var="rb" />
<html>
<head>
    <title><fmt:message key="title.drugs" bundle="${rb}"/></title>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css">
</head>
<body>
    <c:set var="page" value="path.page.drugs" scope="session"/>
    <%@ include file="common/menu.jsp"%>
    <div class="wrapper container">
        <div class="content text-center">
            <h1>
                <fmt:message key="drugs.caption" bundle="${rb}"/>
            </h1>
            <c:forEach var="drug" items="${drugs}">
                <%@ include file="client/short_drug.jsp"%>
            </c:forEach>
        </div>
        <%@ include file="common/footer.jsp"%>
    </div>
</body>
</html>
