<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="resources.pagecontent" var="rb" />

<html>
<head>
    <title><fmt:message key="title.add_recipe" bundle="${rb}"/></title>
    <link rel='stylesheet' href="${pageContext.request.contextPath}/css/style.css" type='text/css'>
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body>
<c:set var="page" value="path.page.add_recipe" scope="session"/>
<%@ include file="../common/menu.jsp"%>

<div class="wrapper container">
    <form class="content form-horizontal" method="POST" action="${pageContext.request.contextPath}/controller">

        <input type="hidden" name="command" value="add_recipe"/>
        <h3>
            <fmt:message key="label.add-recipe" bundle="${rb}"/>
        </h3>

        <div class="form-group">
            <label for="beginning" class="col-sm-2 compulsory control-label">
                <fmt:message key="label.beginning_date" bundle="${rb}"/>
            </label>
            <div class="col-sm-10">
                <input id="beginning" name="beginning" class="form-control" type="date">
            </div>
        </div>
        <div class="form-group">
            <label for="ending" class="col-sm-2 compulsory control-label">
                <fmt:message key="label.ending_date" bundle="${rb}"/>
            </label>
            <div class="col-sm-10">
                <input id="ending" name="ending" class="form-control" type="date">
            </div>
        </div>

        <div class="form-group">
            <label for="client_id" class="col-sm-2 compulsory control-label">
                <fmt:message key="label.client_id" bundle="${rb}"/>
            </label>
            <div class="col-sm-10">
                <input id="client_id" name="client_id" class="form-control" type="number" value="0" required min="1"
                       step="1"
                       placeholder=<fmt:message key="label.client_id" bundle="${rb}"/>>
            </div>
        </div>
        <div class="form-group">
            <label for="drug_id" class="col-sm-2 compulsory control-label">
                <fmt:message key="label.drug_id" bundle="${rb}"/>
            </label>
            <div class="col-sm-10">
                <input id="drug_id" name="drug_id" class="form-control" type="number" value="0" required min="1"
                       step="1"
                       placeholder=<fmt:message key="label.drug_id" bundle="${rb}"/>>
            </div>
        </div>

        <div class="form-group">
            <label for="quantity" class="col-sm-2 compulsory control-label">
                <fmt:message key="label.quantity" bundle="${rb}"/>
            </label>
            <div class="col-sm-10">
                <input id="quantity" name="quantity" class="form-control" type="number" value="0" required min="1"
                       step="1"
                       placeholder=<fmt:message key="label.quantity" bundle="${rb}"/>>
            </div>
        </div>

        <div class="col-sm-offset-3">
            <input type="submit" class="btn btn-success" value=<fmt:message key="button.add-drug" bundle="${rb}"/>>
        </div>
    </form>
    <%@ include file="../common/footer.jsp"%>
</div>
</body>
</html>
