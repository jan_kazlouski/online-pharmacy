<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="resources.pagecontent" var="rb" />

<html>
<head>
    <title>
        <fmt:message key="title.edit_drug" bundle="${rb}"/>
    </title>
    <link rel='stylesheet' href="${pageContext.request.contextPath}/css/style.css" type='text/css'>
</head>
<body>
    <c:set var="page" value="path.page.edit_drug" scope="session"/>
    <%@ include file="../common/menu.jsp"%>

    <div class="wrapper container">
        <form role="form" class="content form-horizontal" method="POST" action="${pageContext.request.contextPath}/controller">
            <input type="hidden" name="command" value="edit_drug"/>
            <input type="hidden" name="drug_id" value="${drug.id}"/>
            <h3>
                <fmt:message key="label.edit_drug" bundle="${rb}"/>
            </h3>
            <div class="form-group">
                <label for="drug_name" class="col-sm-2 compulsory control-label">
                    <fmt:message key="label.drug-name" bundle="${rb}"/>
                </label>
                <div class="col-sm-10">
                    <input id="drug_name" name="drug_name" class="form-control" type="text" required minlength="1" maxlength="50"
                           value="<c:out value="${drug.name}"></c:out>"
                           pattern=".{1,50}"
                           placeholder=<fmt:message key="label.drug-name" bundle="${rb}"/>>
                </div>
            </div>
            <div class="form-group">
                <label for="description" class="col-sm-2 compulsory control-label">
                    <fmt:message key="label.description" bundle="${rb}"/>
                </label>
                <div class="col-sm-10">
                        <textarea id="description" name="description" class="form-control" required maxlength="1000"
                                  placeholder=<fmt:message key="label.description" bundle="${rb}"/>><c:out value="${drug.description}"></c:out></textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="require_recipe" class="col-sm-2 compulsory control-label">
                    <fmt:message key="label.require-recipe" bundle="${rb}"/>
                </label>
                <div class="col-sm-10">
                    <input id="require_recipe" name="require_recipe" value="true" class="form-control" type="checkbox">
                </div>
            </div>

            <div class="form-group">
                <label for="price" class="col-sm-2 compulsory control-label">
                    <fmt:message key="label.price" bundle="${rb}"/>
                </label>
                <div class="col-sm-10">
                    <input id="price" name="price" class="form-control" type="number" value="<c:out value="${drug.price}"></c:out>" required min="0"
                           pattern="^[0-9]+(([\,\.]?[0-9]+)*)?$"
                           placeholder=<fmt:message key="label.price" bundle="${rb}"/>>
                </div>
            </div>

            <div class="form-group">
                <label for="initial_dose" class="col-sm-2 compulsory control-label">
                    <fmt:message key="label.initial-dose" bundle="${rb}"/>
                </label>
                <div class="col-sm-10">
                        <textarea id="initial_dose" name="initial_dose" class="form-control" required maxlength="1000"
                                  placeholder=<fmt:message key="label.initial-dose" bundle="${rb}"/>><c:out value="${drug.initialDose}"></c:out></textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="maintense_dose" class="col-sm-2 compulsory control-label">
                    <fmt:message key="label.maintense-dose" bundle="${rb}"/>
                </label>
                <div class="col-sm-10">
                        <textarea id="maintense_dose" name="maintense_dose" class="form-control" required maxlength="1000"
                                  placeholder=<fmt:message key="label.maintense-dose" bundle="${rb}"/>><c:out value="${drug.maintenseDose}"></c:out></textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="group" class="col-sm-2 compulsory control-label">
                    <fmt:message key="label.group" bundle="${rb}"/>
                </label>
                <div class="col-sm-10">
                    <input id="group" name="group" class="form-control" type="text" value="<c:out value="${group.name}"></c:out>" required minlength="1" maxlength="50"
                           pattern=".{1,50}"
                           placeholder=<fmt:message key="label.group" bundle="${rb}"/>>
                </div>
            </div>

            <div class="form-group">
                <label for="producer" class="col-sm-2 compulsory control-label">
                    <fmt:message key="label.producer" bundle="${rb}"/>
                </label>
                <div class="col-sm-10">
                    <input id="producer" name="producer" class="form-control" type="text" value="<c:out value="${producer.name}"></c:out>" required minlength="1" maxlength="50"
                           pattern=".{1,50}"
                           placeholder=<fmt:message key="label.producer" bundle="${rb}"/>>
                </div>
            </div>

            <div class="col-sm-offset-3">
                <input type="submit" class="btn btn-success" value=<fmt:message key="button.edit" bundle="${rb}"/>>
                <a role="button" href="${pageContext.request.contextPath}/controller?command=show_drug&drug_id=${drug.id}" class="btn btn-default">
                    <fmt:message key="button.cancel" bundle="${rb}"/>
                </a>
            </div>
        </form>
        <%@ include file="../common/footer.jsp"%>
    </div>

</body>
</html>
