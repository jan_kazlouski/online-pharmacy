<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="resources.pagecontent" var="rb" />

<html>
<head>
    <title><fmt:message key="title.add_drug" bundle="${rb}"/></title>
    <link rel='stylesheet' href="${pageContext.request.contextPath}/css/style.css" type='text/css'>
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body>
    <c:set var="page" value="path.page.add_drug" scope="session"/>
    <%@ include file="../common/menu.jsp"%>

    <div class="wrapper container">
        <form class="content form-horizontal" method="POST" action="${pageContext.request.contextPath}/controller">

            <input type="hidden" name="command" value="add_drug"/>
            <h3>
                <fmt:message key="label.add-drug" bundle="${rb}"/>
            </h3>
            <div class="form-group">
                <label for="name" class="col-sm-2 compulsory control-label">
                    <fmt:message key="label.drug-name" bundle="${rb}"/>
                </label>
                <div class="col-sm-10">
                    <input id="name" name="name" class="form-control" type="text" value="" required minlength="1" maxlength="50"
                           pattern=".{1,50}"
                           placeholder=<fmt:message key="label.drug-name" bundle="${rb}"/>>
                </div>
            </div>

            <div class="form-group">
                <label for="description" class="col-sm-2 compulsory control-label">
                    <fmt:message key="label.description" bundle="${rb}"/>
                </label>
                <div class="col-sm-10">
                        <textarea id="description" name="description" class="form-control" required maxlength="1000"
                                  placeholder=<fmt:message key="label.description" bundle="${rb}"/>></textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="require_recipe" class="col-sm-2 compulsory control-label">
                    <fmt:message key="label.require-recipe" bundle="${rb}"/>
                </label>
                <div class="col-sm-10">
                    <input id="require_recipe" name="require_recipe" class="form-control" type="checkbox">
                </div>
            </div>

            <div class="form-group">
                <label for="price" class="col-sm-2 compulsory control-label">
                    <fmt:message key="label.price" bundle="${rb}"/>
                </label>
                <div class="col-sm-10">
                    <input id="price" name="price" class="form-control" type="number" value="0.0" required min="0"
                           pattern="^[0-9]+(([\,\.]?[0-9]+)*)?$"
                           placeholder=<fmt:message key="label.price" bundle="${rb}"/>>
                </div>
            </div>

            <div class="form-group">
                <label for="initial_dose" class="col-sm-2 compulsory control-label">
                    <fmt:message key="label.initial-dose" bundle="${rb}"/>
                </label>
                <div class="col-sm-10">
                        <textarea id="initial_dose" name="initial_dose" class="form-control" required maxlength="1000"
                                  placeholder=<fmt:message key="label.initial-dose" bundle="${rb}"/>></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="maintense_dose" class="col-sm-2 compulsory control-label">
                    <fmt:message key="label.maintense-dose" bundle="${rb}"/>
                </label>
                <div class="col-sm-10">
                        <textarea id="maintense_dose" name="maintense_dose" class="form-control" required maxlength="1000"
                                  placeholder=<fmt:message key="label.maintense-dose" bundle="${rb}"/>></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="group" class="col-sm-2 compulsory control-label">
                    <fmt:message key="label.group" bundle="${rb}"/>
                </label>
                <div class="col-sm-10">
                    <input id="group" name="group" class="form-control" type="text" value="" required minlength="1" maxlength="50"
                           pattern=".{1,50}"
                           placeholder=<fmt:message key="label.group" bundle="${rb}"/>>
                </div>
            </div>
            <div class="form-group">
                <label for="producer" class="col-sm-2 compulsory control-label">
                    <fmt:message key="label.producer" bundle="${rb}"/>
                </label>
                <div class="col-sm-10">
                    <input id="producer" name="producer" class="form-control" type="text" value="" required minlength="1" maxlength="50"
                           pattern=".{1,50}"
                           placeholder=<fmt:message key="label.producer" bundle="${rb}"/>>
                </div>
            </div>

            <div class="col-sm-offset-3">
                <input type="submit" class="btn btn-success" value=<fmt:message key="button.add-drug" bundle="${rb}"/>>
            </div>
        </form>
        <%@ include file="../common/footer.jsp"%>
    </div>
</body>
</html>
