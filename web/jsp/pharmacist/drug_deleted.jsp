<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="resources.pagecontent" var="rb" />

<html>
<head>
    <title>
        <fmt:message key="title.drug-deleted" bundle="${rb}"/>
    </title>
    <link rel='stylesheet' href="${pageContext.request.contextPath}/css/style.css" type='text/css'>
</head>
<body class="deleted">
<div class="wrapper container">
    <div class="content">
        <c:set var="page" value="path.page.deleted" scope="session"/>
        <%@ include file="../common/menu.jsp"%>
        <h2>
            <fmt:message key="label.drug_deleted" bundle="${rb}"/>.
        </h2>
        <a class="btn btn-primary" href="${pageContext.request.contextPath}/controller?command=redirect&nextPage=path.page.main" role="button">
            <fmt:message key="label.to-main" bundle="${rb}"/>
        </a>
    </div>
    <%@ include file="../common/footer.jsp"%>
</div>
</body>
</html>
