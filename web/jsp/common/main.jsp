<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="resources.pagecontent" var="rb" />

<html>
      <head>
            <title><fmt:message key="title.welcome" bundle="${rb}"/></title>
            <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/slider.css">
            <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css">
      </head>
      <body>
            <%@ include file="menu.jsp"%>
            <c:set var="page" value="path.page.main" scope="session"/>
      </body>
      <%@ include file="footer.jsp"%>
</html>
