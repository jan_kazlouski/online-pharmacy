<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<%@ taglib uri="customtags" prefix="ctg"%>

<link rel='stylesheet' href="${pageContext.request.contextPath}/css/bootstrap.min.css" type='text/css' media='all'>
<link rel='stylesheet' href="${pageContext.request.contextPath}/css/style.css" type='text/css' media='all'>
<link rel='stylesheet' href="${pageContext.request.contextPath}/css/menu.css" type='text/css' media='all'>

<nav class="navbar navbar-default container">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li href="#" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <fmt:message key="menu.drugs" bundle="${rb}"/>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <ctg:client>
                            <li>
                                <a href="${pageContext.request.contextPath}/controller?command=available_drugs">
                                    <fmt:message key="menu.drugs.available" bundle="${rb}"/>
                                </a>
                            </li>
                        </ctg:client>
                        <ctg:pharmacist>
                            <li>
                                <a href="${pageContext.request.contextPath}/controller?command=all_drugs">
                                    <fmt:message key="menu.drugs.all" bundle="${rb}"/>
                                </a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/controller?command=redirect&nextPage=path.page.add_drug">
                                    <fmt:message key="menu.drugs.add-drug" bundle="${rb}"/>
                                </a>
                            </li>
                        </ctg:pharmacist>
                    </ul>
                </li>
                <ctg:admin>
                    <li><a href="${pageContext.request.contextPath}/controller?command=users">
                        <fmt:message key="menu.users" bundle="${rb}"/>
                    </a></li>
                </ctg:admin>
                <ctg:admin>
                    <li><a href="${pageContext.request.contextPath}/controller?command=all_orders">
                        <fmt:message key="menu.orders" bundle="${rb}"/>
                    </a></li>
                </ctg:admin>
                <li href="#" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <fmt:message key="menu.recipes" bundle="${rb}"/>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <ctg:doctor>
                            <li><a href="${pageContext.request.contextPath}/controller?command=redirect&nextPage=path.page.add_recipe">
                                <fmt:message key="menu.add-recipe" bundle="${rb}"/>
                            </a></li>
                        </ctg:doctor>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-left" role="search">
                <input type="hidden" name="command" value="search"/>
                <div class="form-group">
                    <input name="query" type="text" class="form-control" placeholder=<fmt:message key="placeholder.search" bundle="${rb}"/>>
                </div>
                <button type="submit" class="btn btn-default"><fmt:message key="button.submit" bundle="${rb}"/></button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <fmt:message key="menu.language" bundle="${rb}"/>
                        <span class="caret"></span>
                    </a>
                    <fmt:setBundle basename="resources.language" var="lang"/>
                    <ul class="dropdown-menu">
                        <li><a href="${pageContext.request.contextPath}/controller?command=change_language&language=en">
                            <fmt:message key="language.english" bundle="${lang}"/>
                        </a></li>
                        <li><a href="${pageContext.request.contextPath}/controller?command=change_language&language=ru">
                            <fmt:message key="language.russian" bundle="${lang}"/>
                        </a></li>
                    </ul>
                    <fmt:setBundle basename="resources.pagecontent" var="rb"/>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/controller?command=logout">
                    <fmt:message key="menu.logout" bundle="${rb}"/>
                </a></li>
            </ul>
        </div>
</nav>
