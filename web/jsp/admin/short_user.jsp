<div class="container content-container">
    <div class="col-sm-6 text-container">
            <h2><c:out value="${user.login}"></c:out></h2>
        <h4 class="sub-text">
            <fmt:message key="user.category" bundle="${rb}"/>: <c:out value="${user.userCategory}"></c:out>
        </h4>
        <table class="info table-hover">
            <tbody>
            <tr>
                <td class="type">
                    <fmt:message key="user.first-name" bundle="${rb}"/>
                </td>
                <td><c:out value="${user.firstName}"></c:out></td>
            </tr>
            <tr>
                <td class="type">
                    <fmt:message key="user.middle-name" bundle="${rb}"/>
                </td>
                <td><c:out value="${user.middleName}"></c:out></td>
            </tr>
            <tr>
                <td class="type">
                    <fmt:message key="user.last-name" bundle="${rb}"/>
                </td>
                <td><c:out value="${user.lastName}"></c:out></td>
            </tr>
            <tr>
                <td class="type">
                    <fmt:message key="user.email" bundle="${rb}"/>
                </td>
                <td><c:out value="${user.email}"></c:out></td>
            </tr>
            </tbody>
        </table>
        <br>
        <a class="btn btn-danger" href="${pageContext.request.contextPath}/controller?command=delete_user&id=${user.id}" role="button">
            <fmt:message key="button.delete" bundle="${rb}"/>
        </a>
        <br>
        <br>
    </div>
</div>
