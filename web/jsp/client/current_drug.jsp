<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="customtags" prefix="ctg"%>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="resources.pagecontent" var="rb" />
<html>
<head>
    <title>${drug.name}</title>
    <link type='text/css' rel='stylesheet' href="${pageContext.request.contextPath}/css/style.css">
</head>
<body>
<c:set var="page" value="path.page.current_drug" scope="session"/>
<%@ include file="../common/menu.jsp"%>
<div class="wrapper container">
    <div class="content">
        <h1 class="section-title">
            <fmt:message key="label.about" bundle="${rb}"/> "<c:out value="${drug.name}"></c:out>"
        </h1>
        <div class="container content-container">
            <h2><c:out value="${drug.name}"></c:out></h2>
            <ctg:pharmacist>
                <a class="btn btn-danger" href="${pageContext.request.contextPath}/controller?command=delete_drug&drug_id=${drug.id}" role="button">
                    <fmt:message key="button.delete" bundle="${rb}"/>
                </a>
                <a class="btn btn-danger" href="${pageContext.request.contextPath}/controller?command=init_edit_drug&drug_id=${drug.id}" role="button">
                    <fmt:message key="button.edit" bundle="${rb}"/>
                </a>
            </ctg:pharmacist>
            <ctg:client>
                <a class="btn btn-danger" href="${pageContext.request.contextPath}/controller?command=init_order&drug_id=${drug.id}" role="button">
                    <fmt:message key="button.order" bundle="${rb}"/>
                </a>
            </ctg:client>
            <br>
            <h4 class="sub-text">
                <fmt:message key="label.description" bundle="${rb}"/>: <c:out value="${drug.description}"></c:out>
            </h4>
            <h4 class="sub-text">
                <fmt:message key="label.price" bundle="${rb}"/>: <c:out value="${drug.price}"></c:out>
            </h4>
            <h4 class="sub-text">
                <fmt:message key="label.initial-dose" bundle="${rb}"/>: <c:out value="${drug.initialDose}"></c:out>
            </h4>
            <h4 class="sub-text">
                <fmt:message key="label.maintense-dose" bundle="${rb}"/>: <c:out value="${drug.maintenseDose}"></c:out>
            </h4>
            <h4 class="sub-text">
                <fmt:message key="label.group" bundle="${rb}"/>: <c:out value="${group.name}"></c:out>
            </h4>
            <h4 class="sub-text">
                <fmt:message key="label.producer" bundle="${rb}"/>: <c:out value="${producer.name}"></c:out>
            </h4>
        </div>
    </div>
    <%@ include file="../common/footer.jsp"%>
</div>
</body>
</html>
