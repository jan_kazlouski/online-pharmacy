<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<fmt:setLocale value="${locale}"/>
<fmt:setBundle basename="resources.pagecontent" var="rb" />

<html>
<head>
    <title><fmt:message key="title.add_order" bundle="${rb}"/></title>
    <link rel='stylesheet' href="${pageContext.request.contextPath}/css/style.css" type='text/css'>
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body>
<c:set var="page" value="path.page.add_order" scope="session"/>
<%@ include file="../common/menu.jsp"%>

<div class="wrapper container">
    <form class="content form-horizontal" method="POST" action="${pageContext.request.contextPath}/controller">

        <input type="hidden" name="drug_id" value="${drug.id}"/>
        <input type="hidden" name="command" value="add_order"/>

        <div class="form-group">
            <label class="col-sm-2 compulsory control-label">
                <fmt:message key="label.drug-name" bundle="${rb}"/>
            </label>
            <div class="col-sm-10">
                ${drug.name}
            </div>
        </div>

        <div class="form-group">
            <label for="quantity" class="col-sm-2 compulsory control-label">
                <fmt:message key="label.quantity" bundle="${rb}"/>
            </label>
            <div class="col-sm-10">
                <input id="quantity" name="quantity" class="form-control" type="number" value="" required min="1"
                       pattern="\d+"
                       placeholder=<fmt:message key="label.quantity" bundle="${rb}"/>>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 compulsory control-label">
                <fmt:message key="label.price" bundle="${rb}"/>
            </label>
            <div class="col-sm-10">
                ${drug.price}
            </div>
        </div>
        <div class="col-sm-offset-3">
            <input type="submit" class="btn btn-success" value=<fmt:message key="button.add-order" bundle="${rb}"/>>
        </div>
    </form>
    <%@ include file="../common/footer.jsp"%>
</div>
</body>
</html>
