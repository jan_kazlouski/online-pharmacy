<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="container content-container">
    <div class="col-sm-6 text-container">
        <a href="${pageContext.request.contextPath}/controller?command=show_drug&drug_id=${drug.id}">
            <h2><c:out value="${drug.name}"></c:out></h2>
        </a>
        <h4 class="sub-text">
            <fmt:message key="label.price" bundle="${rb}"/>: ${drug.price} BYN
        </h4>
        <div class="justify"><c:out value="${drug.description}"></c:out></div>
        <hr>
    </div>
</div>
