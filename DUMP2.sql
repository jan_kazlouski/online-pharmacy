CREATE DATABASE  IF NOT EXISTS `pharmacy` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `pharmacy`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pharmacy
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `drug`
--

DROP TABLE IF EXISTS `drug`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drug` (
  `drug_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL COMMENT '╨Э╨░╨╖╨▓╨░╨╜╨╕╨╡ ╨╝╨╡╨┤╨╕╤Ж╨╕╨╜╤Б╨║╨╛╨│╨╛ ╨┐╤А╨╡╨┐╨░╤А╨░╤В╨░.',
  `description` text,
  `require_recipe` tinyint(4) DEFAULT NULL COMMENT '╨Э╨╡╨╛╨▒╤Е╨╛╨┤╨╕╨╝ ╨╗╨╕ ╨║╨╗╨╕╨╡╨╜╤В╤Г ╤А╨╡╤Ж╨╡╨┐╤В ╨┤╨╗╤П ╨┐╨╛╨║╤Г╨┐╨║╨╕ ╨┐╤А╨╡╨┐╨░╤А╨░╤В╨░.',
  `price` decimal(13,4) DEFAULT NULL,
  `initial_dose` varchar(255) DEFAULT NULL,
  `maintense_dose` varchar(255) DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `producer_id` int(11) NOT NULL,
  PRIMARY KEY (`drug_id`),
  KEY `fk_drug_group1_idx` (`group_id`),
  KEY `fk_drug_pharmaceutical_company1_idx` (`producer_id`),
  CONSTRAINT `fk_drug_group1` FOREIGN KEY (`group_id`) REFERENCES `group` (`group_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_drug_pharmaceutical_company1` FOREIGN KEY (`producer_id`) REFERENCES `producer` (`producer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drug`
--

LOCK TABLES `drug` WRITE;
/*!40000 ALTER TABLE `drug` DISABLE KEYS */;
INSERT INTO `drug` VALUES (1,'Acetaminophen','Acetaminophen is a pain reliever and a fever reducer.',1,31.1000,'1000 milligrams (mg) at one time, or more than 4000 mg in 24 hours.','5 to 10 mg orally once a day',2,7),(2,'Adderall','Adderall contains a combination of amphetamine and dextroamphetamine. Amphetamine and dextroamphetamine are central nervous system stimulants that affect chemicals in the brain and nerves that contribute to hyperactivity control.',0,60.1000,'5 mg orally 1 or 2 times a day','5 mg increments at weekly intervals until optimal response is obtained.',2,5),(3,'Alprazolam','Alprazolam is a benzodiazepine (ben-zoe-dye-AZE-eh-peen). It affects chemicals in the brain that may be unbalanced in people with anxiety.',0,30.1000,'0.25 to 0.5 mg orally 3 times a day','May increase up to maximum daily dose of 4 mg in divided doses',3,3),(4,'Amitriptyline','Amitriptyline is a tricyclic antidepressant. Amitriptyline affects chemicals in the brain that may be unbalanced in people with depression.',0,90.1000,'75 mg orally per day in divided doses; this may be increased to a total of 150 mg per day if needed','40 to 100 mg orally as a single dose at bedtime',4,1),(5,'Amlodipine','Amlodipine is a calcium channel blocker that dilates (widens) blood vessels and improves blood flow.',1,110.1000,'5 mg orally once a day','5 to 10 mg orally once a day',2,2),(6,'Amoxicillin','Amoxicillin is a penicillin antibiotic that fights bacteria.',0,10.1000,'5 mg orally once a day','5 to 10 mg orally once a day',2,6),(7,'Ativan','Ativan (lorazepam) belongs to a group of drugs called benzodiazepines. Lorazepam affects chemicals in the brain that may be unbalanced in people with anxiety.',0,102.1000,'2 to 3 mg orally per day administered 2 to 3 times per day','1 to 2 mg orally 2 to 3 times a day',3,5),(8,'Atorvastatin','Atorvastatin is in a group of drugs called HMG CoA reductase inhibitors, or \"statins.\" Atorvastatin reduces levels of \"bad\" cholesterol (low-density lipoprotein, or LDL) and triglycerides in the blood, while increasing levels of \"good\" cholesterol (high-density lipoprotein, or HDL).',1,100.1000,'10 mg to 80 mg orally once a day.','10 to 80 mg orally once a day.',4,8);
/*!40000 ALTER TABLE `drug` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group`
--

DROP TABLE IF EXISTS `group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group`
--

LOCK TABLES `group` WRITE;
/*!40000 ALTER TABLE `group` DISABLE KEYS */;
INSERT INTO `group` VALUES (2,'Analgesics'),(3,'Antibiotics'),(4,'Antiseptics'),(5,'Painkillers'),(6,'Vitamins'),(7,'Stimulants'),(8,'Hallucinogens');
/*!40000 ALTER TABLE `group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` int(11) DEFAULT NULL COMMENT '╨Ъ╨╛╨╗╨╕╤З╨╡╤Б╤В╨▓╨╛ ╨┐╤А╨╡╨┐╨░╤А╨░╤В╨░ ╨▓ ╨╖╨░╨║╨░╨╖╨╡.',
  `order_date` date DEFAULT NULL COMMENT '╨Ф╨░╤В╨░ ╨╛╤Д╨╛╤А╨╝╨╗╨╡╨╜╨╕╤П ╨╖╨░╨║╨░╨╖╨░.',
  `pirce` decimal(13,4) DEFAULT NULL COMMENT '╨ж╨╡╨╜╨░ ╨╖╨░╨║╨░╨╖╨░.',
  `user_id` int(11) NOT NULL COMMENT '╨Ъ╨╗╨╕╨╡╨╜╤В, ╨╛╤Д╨╛╤А╨╝╨╕╨▓╤И╨╕╨╣ ╨╖╨░╨║╨░╨╖ ╨┐╤А╨╡╨┐╨░╤А╨░╤В╨░.',
  `drug_id` int(11) NOT NULL COMMENT '╨Я╤А╨╡╨┐╨░╤А╨░╤В, ╨║╨╛╤В╨╛╤А╤Л╨╣ ╨╖╨░╨║╨░╨╖╨░╨╗ ╨║╨╗╨╕╨╡╨╜╤В.',
  `order_status_id` int(11) NOT NULL COMMENT '╨б╤В╨░╤В╤Г╤Б ╨╖╨░╨║╨░╨╖╨░. "╨Т ╨╛╤З╨╡╤А╨╡╨┤╨╕", "╨Т ╨╛╨▒╤А╨░╨▒╨╛╤В╨║╨╡", "╨Т╤Л╨┐╨╛╨╗╨╜╨╡╨╜", "╨Ю╤В╨║╨╗╨╛╨╜╨╡╨╜".',
  PRIMARY KEY (`order_id`),
  KEY `fk_Orders_Clients1_idx` (`user_id`),
  KEY `fk_Orders_Drugs1_idx` (`drug_id`),
  KEY `fk_order_status1_idx` (`order_status_id`),
  CONSTRAINT `fk_Orders_Clients1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Orders_Drugs1` FOREIGN KEY (`drug_id`) REFERENCES `drug` (`drug_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_status1` FOREIGN KEY (`order_status_id`) REFERENCES `order_status` (`order_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_status`
--

DROP TABLE IF EXISTS `order_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_status` (
  `order_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL COMMENT '╨Э╨░╨╖╨▓╨░╨╜╨╕╨╡ ╤Б╤В╨░╤В╤Г╤Б╨░ ╨╖╨░╨║╨░╨╖╨░.',
  PRIMARY KEY (`order_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_status`
--

LOCK TABLES `order_status` WRITE;
/*!40000 ALTER TABLE `order_status` DISABLE KEYS */;
INSERT INTO `order_status` VALUES (1,'New'),(2,'In process'),(3,'Processed'),(4,'Rejected');
/*!40000 ALTER TABLE `order_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producer`
--

DROP TABLE IF EXISTS `producer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producer` (
  `producer_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`producer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producer`
--

LOCK TABLES `producer` WRITE;
/*!40000 ALTER TABLE `producer` DISABLE KEYS */;
INSERT INTO `producer` VALUES (1,'GlaxoSmithKline'),(2,'AstraZeneca'),(3,'Gilead'),(5,'Merck'),(6,'Roche'),(7,'Pfizer'),(8,'Novartis'),(9,'Bayer');
/*!40000 ALTER TABLE `producer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recipe`
--

DROP TABLE IF EXISTS `recipe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recipe` (
  `recipe_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_of_beginning` date DEFAULT NULL COMMENT '╨Ф╨░╤В╨░ ╨╜╨░╤З╨░╨╗╨░ ╨┤╨╡╨╣╤Б╤В╨▓╨╕╤П ╤А╨╡╤Ж╨╡╨┐╤В╨░.',
  `date_of_ending` date DEFAULT NULL COMMENT '╨Ф╨░╤В╨░ ╨╛╨║╨╛╨╜╤З╨░╨╜╨╕╤П ╨┤╨╡╨╣╤Б╤В╨▓╨╕╤П ╤А╨╡╤Ж╨╡╨┐╤В╨░.',
  `quantity` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL COMMENT '╨Ъ╨╗╨╕╨╡╨╜╤В, ╨║╨╛╤В╨╛╤А╨╛╨╝╤Г ╨▓╤Л╨┐╨╕╤Б╨░╨╜ ╤А╨╡╤Ж╨╡╨┐╤В ╨╜╨░ ╨┐╤А╨╡╨┐╨░╤А╨░╤В.',
  `doctor_id` int(11) NOT NULL COMMENT '╨Ф╨╛╨║╤В╨╛╤А, ╨║╨╛╤В╨╛╤А╤Л╨╣ ╨▓╤Л╨┐╨╕╤Б╨░╨╗ ╨║╨╗╨╕╨╡╨╜╤В╤Г ╤А╨╡╤Ж╨╡╨┐╤В ╨╜╨░ ╨┐╤А╨╡╨┐╨░╤А╨░╤В.',
  `drug_id` int(11) NOT NULL COMMENT '╨Я╤А╨╡╨┐╨░╤А╨░╤В, ╨╜╨░ ╨║╨╛╤В╨╛╤А╤Л╨╣ ╨▓╤Л╨┐╨╕╤Б╨░╨╜ ╤А╨╡╤Ж╨╡╨┐╤В.',
  PRIMARY KEY (`recipe_id`),
  KEY `fk_Recipes_Clients_idx` (`client_id`),
  KEY `fk_Recipes_Drugs1_idx` (`drug_id`),
  KEY `fk_recipe_user1_idx` (`doctor_id`),
  CONSTRAINT `fk_Recipes_Clients` FOREIGN KEY (`client_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Recipes_Drugs1` FOREIGN KEY (`drug_id`) REFERENCES `drug` (`drug_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_recipe_user1` FOREIGN KEY (`doctor_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recipe`
--

LOCK TABLES `recipe` WRITE;
/*!40000 ALTER TABLE `recipe` DISABLE KEYS */;
INSERT INTO `recipe` VALUES (1,'2012-10-10','2012-11-10',1,2,5,1),(2,'2012-10-12','2019-11-12',2,3,5,5),(3,'2012-10-15','2019-12-15',3,2,5,8);
/*!40000 ALTER TABLE `recipe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `first_name` varchar(45) DEFAULT NULL COMMENT '╨Ш╨╝╤П ╨┐╨╛╨╗╤М╨╖╨╛╨▓╨░╤В╨╡╨╗╤П.',
  `middle_name` varchar(45) DEFAULT NULL COMMENT '╨Ю╤В╤З╨╡╤Б╤В╨▓╨╛ ╨┐╨╛╨╗╤М╨╖╨╛╨▓╨░╤В╨╡╨╗╤П.',
  `last_name` varchar(45) DEFAULT NULL COMMENT '╨д╨░╨╝╨╕╨╗╨╕╤П ╨┐╨╛╨╗╤М╨╖╨╛╨▓╨░╤В╨╡╨╗╤П.',
  `email` varchar(255) DEFAULT NULL COMMENT '╨н╨╗╨╡╨║╤В╤А╨╛╨╜╨╜╤Л╨╣ ╨┐╨╛╤З╤В╨╛╨▓╤Л╨╣ ╤П╤Й╨╕╨║ ╨┐╨╛╨╗╤М╨╖╨╛╨▓╨░╤В╨╡╨╗╤П.',
  `user_category_id` int(11) NOT NULL COMMENT '╨Ъ╨░╤В╨╡╨│╨╛╤А╨╕╤П ╨┐╨╛╨╗╤М╨╖╨╛╨▓╨░╤В╨╡╨╗╤П, ╨║╨╛╤В╨╛╤А╨░╤П ╨╛╨┐╤А╨╡╨┤╨╡╨╗╤П╨╡╤В ╨┐╤А╨╕╨╜╨░╨┤╨╗╨╡╨╢╨╜╨╛╤Б╤В╤М ╨┐╨╛╨╗╤М╨╖╨╛╨▓╨░╤В╨╡╨╗╤П ╨║ ╨╛╨┐╤А╨╡╨┤╨╡╨╗╨╡╨╜╨╜╨╛╨╝╤Г ╨║╤А╤Г╨│╤Г. ╨Ъ╨╗╨╕╨╡╨╜╤В╨╛╨▓, ╨▓╤А╨░╤З╨╡╨╣, ╤Д╨░╤А╨╝╨░╤Ж╨╡╨▓╤В╨╛╨▓, ╨░╨┤╨╝╨╕╨╜╨╕╤Б╤В╤А╨░╤В╨╛╤А╨╛╨▓.',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `login_UNIQUE` (`login`),
  KEY `fk_user_category1_idx` (`user_category_id`),
  CONSTRAINT `fk_user_category1` FOREIGN KEY (`user_category_id`) REFERENCES `user_category` (`user_category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'root','63a9f0ea7bb98050796b649e85481845','Jack','Arthur','Nicholson','root@gmail.com',4),(2,'client1','client1','John','David','Travolta','client1@gmail.com',1),(3,'client2','client2','Chad','Robert','Nicholson','client2@gmail.com',1),(4,'pharmacist','pharmacist','Tyrone','Aiden','Power','pharmacist@gmail.com',2),(5,'doctor','doctor','Nina','Conrad','Dobrev','doctor@gmail.com',3);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_category`
--

DROP TABLE IF EXISTS `user_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_category` (
  `user_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '╨Э╨░╨╖╨▓╨░╨╜╨╕╨╡ ╨║╨░╤В╨╡╨│╨╛╤А╨╕╨╕ ╨┐╨╛╨╗╤М╨╖╨╛╨▓╨░╤В╨╡╨╗╤П.',
  PRIMARY KEY (`user_category_id`),
  UNIQUE KEY `name_UNIQUE` (`name`) COMMENT '╨Ш╨╜╨┤╨╡╨║╤Б ╨┤╨╗╤П ╨╛╨▒╨╡╤Б╨┐╨╡╤З╨╡╨╜╨╕╤П ╤Г╨╜╨╕╨║╨░╨╗╤М╨╜╨╛╤Б╤В╨╕ ╨╖╨╜╨░╤З╨╡╨╜╨╕╨╣ ╨┐╨╛╨╗╤П.'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_category`
--

LOCK TABLES `user_category` WRITE;
/*!40000 ALTER TABLE `user_category` DISABLE KEYS */;
INSERT INTO `user_category` VALUES (4,'Admin'),(1,'Client'),(3,'Doctor'),(2,'Pharmacist');
/*!40000 ALTER TABLE `user_category` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-03  0:31:17
